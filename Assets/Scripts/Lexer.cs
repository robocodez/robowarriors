﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Lexer {

    Dictionary<string, Token> dict;
    string _text;
    int pos;
    char current_char;

    public Lexer(string text)
    {
        dict = new Dictionary<string, Token>{
            { "PROGRAM", new Token("PROGRAM", "PROGRAM") },
            { "VAR", new Token("VAR", "VAR") },
            { "DIV", new Token("INTEGER_DIV", "DIV") },
            { "INTEGER", new Token("INTEGER", "INTEGER") },
            { "REAL", new Token("REAL", "REAL") },
            { "BEGIN", new Token("BEGIN", "BEGIN") },
            { "END", new Token("END", "END") },
            {" PROCEDURE", new Token("PROCEDURE", "PROCEDURE") },
        };
        this._text = text;
        this.pos = 0;
        this.current_char = this._text[this.pos];
    }
	
    public void error()
    {
        try
        {
            throw new Exception();
        }
        
        catch(Exception e)
        {
            Debug.Log("Invalid character");
        }
    }

    public void advance()
    {
        this.pos += 1;
    }
}
