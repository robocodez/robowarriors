﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionChoices : MonoBehaviour {

    private Rect windowRect = new Rect(220, 45, 500, 300);
    private bool conditionPressed = false;
    private bool actionPressed = false;
    private bool backPressed = false;

    private void Update()
    {
        if (conditionPressed)
        {
            //windowRect = GUILayout.Window(0, windowRect, WindowFunction, "My Window");
            conditionPressed = false;
        }
        else if (actionPressed)
        {
            actionPressed = false;
        }
        else if (backPressed)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }

    /*void OnGUI()
    {
        
        



    }*/
    
    void WindowFunction(int windowID)
    {
        if (GUILayout.Button("Hello World"))
        {
            print("Got a click");
        }
        if(GUILayout.Button("Yo"))
        {
            print("Got a yo");
        }
        if(GUILayout.Button("Heyo"))
        {
            print("Got a heyo");
        }
        if(GUILayout.Button("Wah"))
        {
            print("Got a wah");
        }


    }

    public void conditionClick()
    {
        conditionPressed = true;
    }

    public void actionClick()
    {
        actionPressed = true;
    }

    public void backClick()
    {
        backPressed = true;
    }


}
