﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {

    public string target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Inside bullet");
        if (collision.gameObject.tag == target || collision.gameObject.tag == "ShieldBit")
        {
            Debug.Log("Hit");
            GameObject impact = Instantiate(Resources.Load("Explosion 1"), transform.position, transform.rotation) as GameObject;
            Destroy(impact, 3f);
            Destroy(this.gameObject);
        }
        
    }
}
