﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThresholdValueScript : MonoBehaviour {

    public Text text;
    public Slider slider;
    // Use this for initialization
    void Start() {
        slider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }

    // Update is called once per frame
    void Update() {

    }

    void ValueChangeCheck()
    {
        text.text = "Threshold HP:" + ((int)((slider.value)*100)).ToString();
    }
}
