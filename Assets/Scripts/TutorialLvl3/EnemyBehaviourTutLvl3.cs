﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourTutLvl3 : MonoBehaviour {

    private GameObject curr;
    private GameObject targ;
    private Rigidbody2D rigidbody;
    private float nf = 0;
    private float speed = 3;
    // Use this for initialization
    void Start () {
        curr = this.gameObject;
        targ = GameObject.FindWithTag("Player");
        rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 playerDist = (targ.transform.position - curr.transform.position);
        if(playerDist.magnitude > 20)
        {
            Chase();
        }
        else
        {
            Shoot();
        }
        if (!targ.GetComponent<PlayerTank>().getFlagStatus())
        {
            this.gameObject.GetComponent<EnemyTank>().AddjustCurrentHealth(-100);
        }
        
	}

    public void Chase()
    {
        //GameObject targ = OnTriggerEnter2D(col2D);
        //Condition for when there is a target and when there is no target
        if (targ != null)
        {
            rigidbody.velocity = Vector2.zero;
            //Get distance to player
            Vector2 playerDist = (targ.transform.position - curr.transform.position);
            //Debug.Log(playerDist.magnitude);

            Vector2 velocity = Vector2.zero;
            //Debug.Log("Player DETECTED");
            //follow player
            if (playerDist.magnitude > 5)
            {
                velocity += playerDist.normalized * 3;

            }
            else
            {
                rigidbody.velocity = velocity;
            }
            rigidbody.velocity = velocity;
            float angle1 = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
            Quaternion q1 = Quaternion.AngleAxis(angle1, Vector3.forward);
            curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q1, Time.deltaTime * 3);
        }
    }

    public void Shoot()
    {
        float Bullet_Forward_Force = 1000;
        GameObject be = (this.gameObject.transform.GetChild(0)).gameObject;
        Vector2 vd = targ.transform.position - curr.transform.position;
        //player faces target to shoot
        float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w), q, Time.deltaTime * speed);
        if (nf > 10)
        {
            //instantiate and create a bullet object
            GameObject temp_bullet;
            temp_bullet = Instantiate(Resources.Load("Bullet 1 1"), be.transform.position, be.transform.rotation) as GameObject;

            //retrieve the rigidbody component from the instantiated bullet and control it
            Rigidbody2D temp_rigidbody;
            temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
            temp_rigidbody.AddForce(transform.right * Bullet_Forward_Force);
            nf = 0;
            Destroy(temp_bullet, 0.98f);
        }
        else
        {
            nf += 0.2f;
        }
    }
    
}
