﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBehaviourChaser : MonoBehaviour
{

    private GameObject curr;
    private Rigidbody2D rigidbody;
    private GameObject sp;
    private float nf = 0;
    private float speed = 3;
    // Use this for initialization
    void Start()
    {
        curr = this.gameObject;
        sp = GameObject.Find("SpawnPoint");
        rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        List<GameObject> playerArray = GameManager.getPlayers();
        GameObject curr = this.gameObject;
        //ObjectStorage.getPlayer1();
        GameObject targ = getClosest(curr, playerArray);
        rigidbody.velocity = Vector2.zero;
        if(targ == null)
        {
            Debug.Log("no player");
        }
        
        Vector2 playerDist = (targ.transform.position - curr.transform.position);
        if (playerDist.magnitude > 10)
        {
            Chase(targ);
        }
        Shoot(targ);
        if (sp.GetComponent<SpawnPointScript>().getScore() >= 1)
        {
            this.gameObject.GetComponent<EnemyTank>().AddjustCurrentHealth(-100);
        }

    }

    public void Chase(GameObject targ)
    {
        //GameObject targ = OnTriggerEnter2D(col2D);
        //Condition for when there is a target and when there is no target
        

        if (targ != null)
        {
            rigidbody.velocity = Vector2.zero;
            //Get distance to player
            Vector2 playerDist = (targ.transform.position - curr.transform.position);
            //Debug.Log(playerDist.magnitude);

            Vector2 velocity = Vector2.zero;
            //Debug.Log("Player DETECTED");
            //follow player
            if (playerDist.magnitude > 5)
            {
                velocity += playerDist.normalized * 3;

            }
            else
            {
                rigidbody.velocity = velocity;
            }
            rigidbody.velocity = velocity;
            float angle1 = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
            Quaternion q1 = Quaternion.AngleAxis(angle1, Vector3.forward);
            curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q1, Time.deltaTime * 3);
        }
    }

    public void Shoot(GameObject targ)
    {
        float Bullet_Forward_Force = 1000;
        GameObject be = (this.gameObject.transform.GetChild(0)).gameObject;
        Vector2 vd = targ.transform.position - curr.transform.position;
        //player faces target to shoot
        float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w), q, Time.deltaTime * speed);
        if (nf > 10)
        {
            //instantiate and create a bullet object
            GameObject temp_bullet;
            temp_bullet = Instantiate(Resources.Load("Bullet 1 1"), be.transform.position, be.transform.rotation) as GameObject;

            //retrieve the rigidbody component from the instantiated bullet and control it
            Rigidbody2D temp_rigidbody;
            temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
            temp_rigidbody.AddForce(transform.right * Bullet_Forward_Force);
            nf = 0;
            Destroy(temp_bullet, 0.98f);
        }
        else
        {
            nf += 0.2f;
        }
    }

    public GameObject getClosest(GameObject curr, List<GameObject> list)
    {
        Vector3 playerPosition = curr.transform.position;
        GameObject closestTarget = null;
        float oldDistance = Mathf.Infinity;
        foreach (GameObject go in list)
        {
            //the first step in the for loop is to find the distance between the player and the current GameObject we are solving for in the Array
            Vector3 distanceDifference = go.transform.position - playerPosition;

            //we set the float currentDistance to the square magnitude of the distanceDifference.  Since distanceDifference is a Vector 3, sqrMagnitude helps us find the float value
            float currentDistance = distanceDifference.sqrMagnitude;

            /*now that we have the currentDistance of the Hazard[i] we need to check three conditions
            1st condition - We need to compare the oldDistance of the last Hazard in the array to the currentDistance of the current Hazard in the array we are solving for
            2nd condition - We need to acces the current Hazard's IsTargetted componet.  It has a bool variable called isTargeted.  If the hazard hasn't been targeted its all good.
            3rd condition - We don't want missiles to launch backwards from the ship.  So we compare the player's position to the hazard's position and make sure the hazards are in about 5 units in front.
            if all 3 of the conditions are true....We have found our new closest Target.*/

            if (currentDistance < oldDistance)
            //go.GetComponent<IsTargeted>().isTargeted == false)
            //&& playerPosition.z < go.transform.position.z - 5)
            {

                //this sets the closestTarget to the current Hazard we are solving for in our array
                closestTarget = go;

                //this sets the oldDistance to the currentDistance for the next cycle through the loop.  If the next Hazard in the array is closer, this will let us know
                oldDistance = currentDistance;

                //when we find the closest target we need to change the Hazard's IsTargetted componet to isTargeted true.  This keeps missiles from targetting already targetted Hazards.
                go.GetComponent<IsTargeted>().isTargeted = true;
            }
        }
        return closestTarget;
    }
}
