﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIDisplay : MonoBehaviour {

    private Dropdown dd;

    public GameObject panelToAttachStuff;
    public GameObject optionsPrefab;

    List<DropdownObject> codes;
    // Use this for initialization
    void Start () {
        dd = GameObject.Find("PlayerDropdown").GetComponent<Dropdown>();
        dd.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        codes = NewAIConfig.getCodes(1);
        //recreateCodes();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void ValueChangeCheck()
    {
        foreach (Transform child in panelToAttachStuff.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        string currPlayer = dd.transform.GetChild(0).GetComponent<Text>().text;
        if (currPlayer.Equals("Player1"))
        {
            codes = NewAIConfig.getCodes(1);
            recreateCodes();
        }
        if (currPlayer.Equals("Player2"))
        {
            codes = NewAIConfig.getCodes(2);
            recreateCodes();

        }
        if (currPlayer.Equals("Player3"))
        {
            codes = NewAIConfig.getCodes(3);
            recreateCodes();
        }
        if (currPlayer.Equals("Player4"))
        {
            codes = NewAIConfig.getCodes(4);
            recreateCodes();
        }
        if (currPlayer.Equals("Player5"))
        {
            codes = NewAIConfig.getCodes(5);
            recreateCodes();
        }

    }

    void recreateCodes()
    {
        foreach (DropdownObject code in codes)
        {
            GameObject temp = createDropdown();
            temp.transform.GetChild(0).GetComponent<Text>().text = code.getCurrCode();//Changing text
            temp.GetComponent<DropdownProperties>().setOldCode(code);
            //temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { newOnValueChanged(temp); });//Setting what dropdown does when valueChanged
            //Debug.Log(code.getCurrCode());
        }
    }

    GameObject createDropdown()
    {
        GameObject options = (GameObject)Instantiate(optionsPrefab);
        options.transform.SetParent(panelToAttachStuff.transform);//Setting button parent
                                                                  //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                  //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        options.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "" });
        // Select it
        options.GetComponent<Dropdown>().value = options.GetComponent<Dropdown>().options.Count - 1;
        // Remove it
        options.GetComponent<Dropdown>().options.RemoveAt(options.GetComponent<Dropdown>().options.Count - 1);
        options.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text
        return options;
    }
}
