﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDAnimationScript : MonoBehaviour {

    public GameObject p1;
    public GameObject p2;
    public GameObject p3;
    public GameObject p4;
    public GameObject p5;
    public GameObject p6;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		p1.transform.Rotate(0, 0, 20 * Time.deltaTime);
        p2.transform.Rotate(0, 0, -20 * Time.deltaTime);
        p3.transform.Rotate(0, 0, 20 * Time.deltaTime);
        p4.transform.Rotate(0, 0, -20 * Time.deltaTime);
        p5.transform.Rotate(0, 0, 20 * Time.deltaTime);
        p6.transform.Rotate(0, 0, -20 * Time.deltaTime);
    }
}
