﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NewAIConfig : MonoBehaviour
{

    //Initialise all codes arrays
    private static List<DropdownObject> codes;
    private List<DropdownObject> storedCodes;

    private static List<DropdownObject> codes1;
    private static List<DropdownObject> codes2;
    private static List<DropdownObject> codes3;
    private static List<DropdownObject> codes4;
    private static List<DropdownObject> codes5;

    private Dropdown dd;
    private GameObject temp;

    private string currPlayer;

    public GameObject panelToAttachStuff;
    public GameObject optionsPrefab;
    public GameObject optionsPrefab2;
    public GameObject optionsTarget;
    public GameObject distSlider;
    public GameObject distText;
    public string gamelevel;

    private static GameObject curHUD;
    private static GameObject playerHUD;
    private static GameObject curRange;
    private static GameObject curRange1;
    private static GameObject curRange2;
    private static GameObject curRange3;
    private static GameObject curRange4;
    private static GameObject curRange5;
    private static GameObject range;

    public GameObject curUI;
    public GameObject afterUI;

    // Use this for initialization
    private void Awake()
    {
        codes1 = new List<DropdownObject>();
        codes2 = new List<DropdownObject>();
        codes3 = new List<DropdownObject>();
        codes4 = new List<DropdownObject>();
        codes5 = new List<DropdownObject>();
        if(optionsPrefab == null)
        {
            Debug.Log("No have");
        }
        //optionsPrefab.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
        //optionsPrefab.transform.SetParent(optionsPrefab.transform.parent.transform, false);
    }
    void Start()
    {
        Debug.Log("NEW");
        //optionsPrefab.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
        if(SavedData.getCodes1() == null)
        {
            Debug.Log("Missing");
        }
        if (SavedData.getCodes1() != null)
        {
            codes1 = SavedData.getCodes1();
            
        }
        if (SavedData.getCodes2() != null)
        {
            codes2 = SavedData.getCodes2();
        }
        if (SavedData.getCodes3() != null)
        {
            codes3 = SavedData.getCodes3();
        }
        if (SavedData.getCodes4() != null)
        {
            codes4 = SavedData.getCodes4();
        }
        if (SavedData.getCodes5() != null)
        {
            codes5 = SavedData.getCodes5();
        }
        codes = codes1;
        recreateCodes();
        temp = createFirstDropdown();
        temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
        if(curUI != null || afterUI != null)
        {
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { nextUI(); });
        }
       

        
        dd = GameObject.Find("PlayerDropdown").GetComponent<Dropdown>();
        dd.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        dd.gameObject.GetComponent<Dropdown>().image.color = new Color(((float)34) / 255, ((float)178) / 255, ((float)178) / 255);
        currPlayer = "Player1";
        GameObject curTank = GameObject.Find(currPlayer);
        playerHUD = Instantiate(Resources.Load("playerHUD"), curTank.transform.position, curTank.transform.rotation) as GameObject;
        //optionsPrefab.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
        MusicScript.changeMusic(2);
        storedCodes = null;
    }

    void nextUI()
    {   
        if(afterUI != null)
        {
            afterUI.SetActive(true);
        }
        if(curUI != null)
        {
            curUI.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
               // Debug.Log("Something was clicked!");
            }
        }
        
    }

    void ValueChangeCheck()
    {   
        if(dd.transform.GetChild(0).GetComponent<Text>().text == "Blue")
        {
            dd.transform.GetChild(0).GetComponent<Text>().text = "Player1";
        }
        if (dd.transform.GetChild(0).GetComponent<Text>().text == "Green")
        {
            dd.transform.GetChild(0).GetComponent<Text>().text = "Player2";
        }
        if (dd.transform.GetChild(0).GetComponent<Text>().text == "Yellow")
        {
            dd.transform.GetChild(0).GetComponent<Text>().text = "Player3";
        }
        if (dd.transform.GetChild(0).GetComponent<Text>().text == "Pink")
        {
            dd.transform.GetChild(0).GetComponent<Text>().text = "Player4";
        }
        if (dd.transform.GetChild(0).GetComponent<Text>().text == "White")
        {
            dd.transform.GetChild(0).GetComponent<Text>().text = "Player5";
        }
        Destroy(curHUD);
        if(curRange != null)
        {
            curRange.SetActive(false);
        }
        
        foreach (Transform child in panelToAttachStuff.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        currPlayer = dd.transform.GetChild(0).GetComponent<Text>().text;
        if (currPlayer.Equals("Player1"))
        {
            dd.gameObject.GetComponent<Dropdown>().image.color = new Color(((float)34) / 255, ((float)178) / 255, ((float)178) / 255);
            codes = codes1;
            recreateCodes();
            if(curRange1 != null)
            {
                curRange1.SetActive(true);
            }
            curRange = curRange1;
            temp = createFirstDropdown();
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
        }
        if (currPlayer.Equals("Player2"))
        {
            dd.gameObject.GetComponent<Dropdown>().image.color = new Color(((float)0) / 255, ((float)255) / 255, ((float)33) / 255);
            codes = codes2;
            recreateCodes();
            if(curRange2 != null)
            {
                curRange2.SetActive(true);
            }
            curRange = curRange2;
            temp = createFirstDropdown();
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked

        }
        if (currPlayer.Equals("Player3"))
        {
            dd.gameObject.GetComponent<Dropdown>().image.color = new Color(((float)255) / 255, ((float)248) / 255, ((float)0) / 255);
            codes = codes3;
            recreateCodes();
            if(curRange3 != null){
                curRange3.SetActive(true);
            }
            curRange = curRange3;
            temp = createFirstDropdown();
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
        }
        if (currPlayer.Equals("Player4"))
        {
            dd.gameObject.GetComponent<Dropdown>().image.color = new Color(((float)255) / 255, ((float)0) / 255, ((float)206) / 255);
            codes = codes4;
            recreateCodes();
            if(curRange4 != null)
            {
                curRange4.SetActive(true);
            }
            curRange = curRange4;
            temp = createFirstDropdown();
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
        }
        if (currPlayer.Equals("Player5"))
        {
            codes = codes5;
            recreateCodes();
            if(curRange5 != null)
            {
                curRange5.SetActive(true);
            }
            curRange = curRange5;
            temp = createFirstDropdown();
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
        }
        Destroy(playerHUD);
        GameObject curTank = GameObject.Find(currPlayer);
        playerHUD = Instantiate(Resources.Load("playerHUD"), curTank.transform.position, curTank.transform.rotation) as GameObject;
    }

    GameObject createSlider()
    {
        GameObject slider = (GameObject)Instantiate(distSlider);
        slider.transform.SetParent(panelToAttachStuff.transform);
        return slider;
    }

    GameObject createText()
    {
        GameObject text = (GameObject)Instantiate(distText);
        text.transform.SetParent(panelToAttachStuff.transform);
        return text;
    }

    GameObject createFirstDropdown()
    {
        GameObject options = (GameObject)Instantiate(optionsPrefab);
        options.transform.SetParent(panelToAttachStuff.transform, false);//Setting button parent
                                                                  //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                  //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        if (options == null)
        {
            Debug.Log("options is null");
        }
        if (ObjectStorage.getList() == null)
        {
            Debug.Log("list is null");
        }
        //options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        options.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "" });
        // Select it
        options.GetComponent<Dropdown>().value = options.GetComponent<Dropdown>().options.Count - 1;
        // Remove it
        options.GetComponent<Dropdown>().options.RemoveAt(options.GetComponent<Dropdown>().options.Count - 1);
        options.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text

        return options;
    }

    GameObject createReplacementDropdown()
    {
        GameObject options = (GameObject)Instantiate(optionsPrefab2);
        options.transform.SetParent(panelToAttachStuff.transform, false);//Setting button parent
                                                                         //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                         //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        if (options == null)
        {
            Debug.Log("options is null");
        }
        if (ObjectStorage.getList() == null)
        {
            Debug.Log("list is null");
        }
        //options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        //options.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "" });
        // Select it
        //options.GetComponent<Dropdown>().value = options.GetComponent<Dropdown>().options.Count - 1;
        // Remove it
        //options.GetComponent<Dropdown>().options.RemoveAt(options.GetComponent<Dropdown>().options.Count - 1);
        options.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text
        //options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        options.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "" });
        // Select it
        options.GetComponent<Dropdown>().value = options.GetComponent<Dropdown>().options.Count - 1;
        // Remove it
        options.GetComponent<Dropdown>().options.RemoveAt(options.GetComponent<Dropdown>().options.Count - 1);
        options.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text
        return options;
    }

    GameObject createDropdownTarget()
    {
        GameObject options = (GameObject)Instantiate(optionsTarget);
        options.transform.SetParent(panelToAttachStuff.transform, false);
        //options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        options.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "" });
        // Select it
        options.GetComponent<Dropdown>().value = options.GetComponent<Dropdown>().options.Count - 1;
        // Remove it
        options.GetComponent<Dropdown>().options.RemoveAt(options.GetComponent<Dropdown>().options.Count - 1);
        options.transform.GetChild(0).GetComponent<Text>().text = "Select Target";//Changing text
        return options;
    }

    void OnValueChanged()
    {
        string currCode = temp.transform.GetChild(0).GetComponent<Text>().text;
        GameObject tempText = null;
        GameObject tempSlider = null;
        GameObject newDd = createReplacementDropdown();
        newDd.GetComponent<Dropdown>().value = temp.GetComponent<Dropdown>().value;
        newDd.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { newOnValueChanged(newDd); });//Setting what dropdown does when valueChanged
        newDd.transform.GetChild(0).GetComponent<Text>().text = currCode;//Changing text
        if (currCode.Equals("stopAt();"))
        {
            tempText = createText();
            tempText.GetComponent<Text>().text = "Stop dist: 3";
            tempSlider = createSlider();
            currCode = "stopAt(3);";
            tempSlider.GetComponent<Slider>().value = (float)(3) / 20;
            tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(newDd, tempSlider); });
        }
        if (currCode.Equals("setThresholdAt();"))
        {
            tempText = createText();
            tempText.GetComponent<Text>().text = "Threshold Hp: 50";
            tempSlider = createSlider();
            currCode = "setThresholdAt(50);";
            tempSlider.GetComponent<Slider>().value = (float)(50) / 100;
            tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedThresholdHp(newDd, tempSlider); });
        }
        if (currCode.Equals("setTarget();"))
        {
            GameObject targetDd = createDropdownTarget();
            targetDd.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnTargetChange(newDd, targetDd); });
            currCode = "setTarget(\"Enemy\")";
            string[] tokens = currCode.Split('"');
            Debug.Log(tokens[1]);
            newDd.GetComponent<DropdownProperties>().setTarget(targetDd);
        }
        DropdownObject ddObj = new DropdownObject(currCode);
        codes.Add(ddObj);
        Destroy(temp);
        newDd.GetComponent<DropdownProperties>().setOldCode(ddObj);
        newDd.GetComponent<DropdownProperties>().setText(tempText);
        newDd.GetComponent<DropdownProperties>().setSlider(tempSlider);
        /*if (tempSlider != null)
        {
            tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChanged(newDd, tempSlider); });
        }*/
        temp = createFirstDropdown();
        Debug.Log(temp.GetComponent<Dropdown>().value);
        temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what dropdown does when valueChanged

    }

    void OnTargetChange(GameObject go, GameObject targetgo)
    {
        GameObject player = GameObject.Find(currPlayer);
        DropdownObject oldCode = go.GetComponent<DropdownProperties>().getOldCode();
        int oldCodeIndex = codes.IndexOf(oldCode);
        Debug.Log(targetgo.transform.GetChild(0).GetComponent<Text>().text);
        DropdownObject tempDd = new DropdownObject("setTarget(\"" + targetgo.transform.GetChild(0).GetComponent<Text>().text + "\");");
        codes[oldCodeIndex] = tempDd;
        go.GetComponent<DropdownProperties>().setOldCode(tempDd);
        GameObject curTarget = GameObject.Find(targetgo.transform.GetChild(0).GetComponent<Text>().text);
        if(curHUD != null)
        {
            Destroy(curHUD);
        }
        curHUD = Instantiate(Resources.Load("HUD"), curTarget.transform.position, curTarget.transform.rotation) as GameObject;
        targetgo.transform.GetChild(0).GetComponent<Text>().text = /*"Target: " + */targetgo.transform.GetChild(0).GetComponent<Text>().text;
        
    }

    void sliderOnValueChangedStopAt(GameObject go, GameObject currSlider)
    {   
        if(curRange != null)
        {
            curRange.SetActive(true);
        }
        GameObject player = GameObject.Find(currPlayer);
        if (curRange == null)
        {
            curRange = Instantiate(Resources.Load("Range"), player.transform.position, player.transform.rotation) as GameObject;
            curRange.transform.SetParent(player.transform);
            curRange.transform.SetSiblingIndex(1);
        }
        else
        {
            curRange.transform.localScale = new Vector3(1, 1, 1) * (int)(currSlider.GetComponent<Slider>().value * 22);
        }
        
        if (currPlayer == "Player1")
        {
            curRange1 = curRange;
        }
        if (currPlayer == "Player2")
        {
            curRange2 = curRange;
        }
        if (currPlayer == "Player3")
        {
            curRange3 = curRange;
        }
        if (currPlayer == "Player4")
        {
            curRange4 = curRange;
        }
        if (currPlayer == "Player5")
        {
            curRange5 = curRange;
        }
        DropdownObject oldCode = go.GetComponent<DropdownProperties>().getOldCode();
        int oldCodeIndex = codes.IndexOf(oldCode);
        DropdownObject tempDd = new DropdownObject("stopAt(" + (int)(currSlider.GetComponent<Slider>().value * 20) + ");");
        go.GetComponent<DropdownProperties>().getText().GetComponent<Text>().text = "Stop Dist: " + (int)(currSlider.GetComponent<Slider>().value * 20);
        codes[oldCodeIndex] = tempDd;
        go.GetComponent<DropdownProperties>().setOldCode(tempDd);
    }

    void sliderOnValueChangedThresholdHp(GameObject go, GameObject currSlider)
    {
        GameObject player = GameObject.Find(currPlayer);
        //player.transform.GetChild(1).transform.localScale = new Vector3(1, 1, 1) * (int)(currSlider.GetComponent<Slider>().value * 22);
        DropdownObject oldCode = go.GetComponent<DropdownProperties>().getOldCode();
        int oldCodeIndex = codes.IndexOf(oldCode);
        DropdownObject tempDd = new DropdownObject("setThresholdAt(" + (int)(currSlider.GetComponent<Slider>().value * 100) + ");");
        go.GetComponent<DropdownProperties>().getText().GetComponent<Text>().text = "Threshold Hp: " + (int)(currSlider.GetComponent<Slider>().value * 100);
        codes[oldCodeIndex] = tempDd;
        go.GetComponent<DropdownProperties>().setOldCode(tempDd);
    }

    void newOnValueChanged(GameObject go)
    {
        int currPos = go.transform.GetSiblingIndex();
        DropdownObject oldCode = go.GetComponent<DropdownProperties>().getOldCode();
        int oldCodeIndex = codes.IndexOf(oldCode);
        DropdownObject tempDd = new DropdownObject(go.transform.GetChild(0).GetComponent<Text>().text);
        if (oldCode.getCurrCode().Substring(0, 6).Equals("setThr") || oldCode.getCurrCode().Substring(0, 6).Equals("stopAt"))
        {
            Destroy(go.GetComponent<DropdownProperties>().getSlider());
            Destroy(go.GetComponent<DropdownProperties>().getText());
            if(oldCode.getCurrCode().Substring(0, 6).Equals("stopAt"))
            {
                GameObject player = GameObject.Find(currPlayer);
                player.transform.GetChild(1).transform.localScale = new Vector3(1, 1, 1) * (int)(0);
            }
            
        }
        if(oldCode.getCurrCode().Substring(0, 6).Equals("setTar"))
        {
            Destroy(go.GetComponent<DropdownProperties>().getTarget());
            Destroy(curHUD);
        }

        if (go.transform.GetChild(0).GetComponent<Text>().text.Equals("setThresholdAt();"))
        {
            GameObject tempText = createText();
            tempText.transform.SetSiblingIndex(currPos + 1);
            tempText.GetComponent<Text>().text = "Threshold Hp: 50";
            GameObject tempSlider = createSlider();
            tempSlider.transform.SetSiblingIndex(currPos + 2);
            tempSlider.GetComponent<Slider>().value = (float)(50) / 100;
            tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedThresholdHp(go, tempSlider); });
            go.GetComponent<DropdownProperties>().setText(tempText);
            go.GetComponent<DropdownProperties>().setSlider(tempSlider);
            tempDd = new DropdownObject("setThresholdAt(50);");
            
        }
        /*if (oldCode.getCurrCode().Substring(0, 6).Equals("stopAt"))
        {
            Destroy(go.GetComponent<DropdownProperties>().getSlider());
            Destroy(go.GetComponent<DropdownProperties>().getText());
            Debug.Log("2 Delete");
        }*/
        if (go.transform.GetChild(0).GetComponent<Text>().text.Equals("stopAt();"))
        {
            GameObject tempText = createText();
            tempText.transform.SetSiblingIndex(currPos + 1);
            tempText.GetComponent<Text>().text = "Stop Dist: 3";
            GameObject tempSlider = createSlider();
            tempSlider.transform.SetSiblingIndex(currPos + 2);
            tempSlider.GetComponent<Slider>().value = (float)(3) / 20;
            tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(go, tempSlider); });
            go.GetComponent<DropdownProperties>().setText(tempText);
            go.GetComponent<DropdownProperties>().setSlider(tempSlider);
            tempDd = new DropdownObject("stopAt(3);");
            
        }
        
        /*if (oldCode.getCurrCode().Substring(0, 6).Equals("setTar"))
        {
            Destroy(go.GetComponent<DropdownProperties>().getSlider());
            Destroy(go.GetComponent<DropdownProperties>().getText());
        }*/
        if (go.transform.GetChild(0).GetComponent<Text>().text.Equals("setTarget();"))
        {
            //GameObject tempText = createText();
            //tempText.transform.SetSiblingIndex(currPos + 1);
            //tempText.GetComponent<Text>().text = "Threshold Hp:";
            //GameObject tempSlider = createSlider();
            //tempSlider.transform.SetSiblingIndex(currPos + 2);
            //tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedThresholdHp(go, tempSlider); });
            //go.GetComponent<DropdownProperties>().setText(tempText);
            //go.GetComponent<DropdownProperties>().setSlider(tempSlider);
            GameObject targetDd = createDropdownTarget();
            targetDd.transform.SetSiblingIndex(currPos + 1);
            targetDd.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnTargetChange(go, targetDd); });
            tempDd = new DropdownObject("setTarget(\"Enemy\");");
            go.GetComponent<DropdownProperties>().setTarget(targetDd);
        }
        codes[oldCodeIndex] = tempDd;
        go.GetComponent<DropdownProperties>().setOldCode(tempDd);
        Debug.Log(temp.GetComponent<Dropdown>().value);
    }

    public static void delete()
    {
        GameObject curBtn = EventSystem.current.currentSelectedGameObject;
        DropdownObject oldCode = curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getOldCode();
        if(oldCode.getCurrCode().Substring(0, 6) == "stopAt")
        {
            Destroy(curRange);
        }
        int oldCodeIndex = codes.IndexOf(oldCode);
        codes.RemoveAt(oldCodeIndex);
        if(curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getTarget() != null)
        {
            Destroy(curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getTarget());
            Destroy(curHUD);
        }
        if (curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getText() != null)
        {
            Destroy(curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getText());
        }
        if (curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getSlider() != null)
        {
            Destroy(curBtn.transform.parent.gameObject.GetComponent<DropdownProperties>().getSlider());
        }
        Destroy(curBtn.transform.parent.gameObject);
        Debug.Log(codes.Count);
    }

    void recreateCodes()
    {
        int count = 0;
        Debug.Log(codes.Count);
        foreach (DropdownObject code in codes)
        {
            GameObject temp = createReplacementDropdown();
            //tempDd.GetComponent<Dropdown>().value = temp.GetComponent<Dropdown>().value;
            int tempFirst = code.getCurrCode().IndexOf('(');
            int tempSecond = code.getCurrCode().IndexOf(')');
            temp.transform.GetChild(0).GetComponent<Text>().text = code.getCurrCode().Substring(0, tempFirst + 1) + code.getCurrCode().Substring(tempSecond);//Changing text
            Debug.Log(temp.GetComponent<Dropdown>().value);
            if (code.getCurrCode().Substring(0, 6).Equals("stopAt"))
            {
                GameObject tempText = createText();
                int first = code.getCurrCode().IndexOf('(');
                int second = code.getCurrCode().IndexOf(')');

                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                int currVal = int.Parse(code.getCurrCode().Substring(first + 1, second - first - 1));
                Debug.Log(currVal);
                tempText.GetComponent<Text>().text = "Stop Dist: " + currVal;
                GameObject tempSlider = createSlider();
                tempSlider.GetComponent<Slider>().value = (float)(currVal) / 20;
                temp.GetComponent<DropdownProperties>().setText(tempText);
                temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(temp, tempSlider); });
            }
            if (code.getCurrCode().Substring(0, 6).Equals("setThr"))
            {
                GameObject tempText = createText();
                int first = code.getCurrCode().IndexOf('(');
                int second = code.getCurrCode().IndexOf(')');

                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                int currVal = int.Parse(code.getCurrCode().Substring(first + 1, second - first - 1));
                Debug.Log(currVal);
                tempText.GetComponent<Text>().text = "Threshold Hp: " + currVal;
                GameObject tempSlider = createSlider();
                tempSlider.GetComponent<Slider>().value = (float)(currVal) / 100;
                temp.GetComponent<DropdownProperties>().setText(tempText);
                temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedThresholdHp(temp, tempSlider); });
            }
            if (code.getCurrCode().Substring(0, 6).Equals("setTar"))
            {
                GameObject tempTarget = createDropdownTarget();
                tempTarget.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnTargetChange(temp, tempTarget); });
                int first = code.getCurrCode().IndexOf("\"");
                int second = code.getCurrCode().IndexOf("\"", first + 1);

                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                string currVal = code.getCurrCode().Substring(first + 1, second - first - 1);
                tempTarget.transform.GetChild(0).GetComponent<Text>().text = currVal;
                temp.GetComponent<DropdownProperties>().setTarget(tempTarget);
                Debug.Log(code.getChecked());
                //Debug.Log("I'MMMMMMMMMMMMMMMMMMMMMMMMMMMM INNNNNNNNNNNNNNNNNNNNNNNNNNNNN");
                //GameObject tempSlider = createSlider();
                //tempSlider.GetComponent<Slider>().value = (float)(currVal) / 20;
                //temp.GetComponent<DropdownProperties>().setText(tempText);
                //temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                //tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(temp, tempSlider); });
            }
            temp.GetComponent<DropdownProperties>().setOldCode(code);
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { newOnValueChanged(temp); });//Setting what dropdown does when valueChanged
            count++;
            Debug.Log(count);
        }
    }

    public static List<DropdownObject> getCodes(int playerNum)
    {
        List<DropdownObject> currCodes = null;
        if (codes1 == null)
        {
            Debug.Log("codes1 is null");
        }
        if (playerNum == 1)
        {
            currCodes = codes1;
            // Debug.Log(playerNum);
        }
        if (playerNum == 2)
        {
            currCodes = codes2;
            // Debug.Log(playerNum);
        }
        if (playerNum == 3)
        {
            currCodes = codes3;
            //Debug.Log(playerNum);
        }
        if (playerNum == 4)
        {
            currCodes = codes4;
            //Debug.Log(playerNum);
        }
        if (playerNum == 5)
        {
            currCodes = codes5;
            //Debug.Log(playerNum);
        }
        if (currCodes == null)
        {
            Debug.Log(playerNum);
        }
        return currCodes;
    }

    public void newGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(gamelevel);
        SavedData.save(codes1, codes2, codes3, codes4, codes5);
    }

    public void reset()
    {
        Destroy(curHUD);
        if (curRange != null)
        {
            curRange.SetActive(false);
        }
        GameObject player = GameObject.Find(currPlayer);
        player.transform.GetChild(1).transform.localScale = new Vector3(1, 1, 1) * (int)(0);
        foreach (Transform child in panelToAttachStuff.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        if (currPlayer.Equals("Player1"))
        {   
            codes1.Clear();
        }
        if (currPlayer.Equals("Player2"))
        {
            codes2.Clear();
        }
        if (currPlayer.Equals("Player3"))
        {
            codes3.Clear();
        }
        if (currPlayer.Equals("Player4"))
        {
            codes4.Clear();
        }
        if (currPlayer.Equals("Player5"))
        {
            codes5.Clear();
        }
        optionsPrefab.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
        temp = createFirstDropdown();
        temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked

        
        dd = GameObject.Find("PlayerDropdown").GetComponent<Dropdown>();
        dd.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        //optionsPrefab.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);//This works
    }

    public void copy()
    {
        storedCodes = new List<DropdownObject>();
        foreach(DropdownObject code in codes)
        {
            storedCodes.Add(code);
        }
    }

    public void paste()
    {
        if (currPlayer.Equals("Player1"))
        {
            codes1.Clear();
        }
        if (currPlayer.Equals("Player2"))
        {
            codes2.Clear();
        }
        if (currPlayer.Equals("Player3"))
        {
            codes3.Clear();
        }
        if (currPlayer.Equals("Player4"))
        {
            codes4.Clear();
        }
        if (currPlayer.Equals("Player5"))
        {
            codes5.Clear();
        }
        if (curRange != null)
        {
            Destroy(curRange);
            Debug.Log("Destroy");
        }
        if (storedCodes == null)
        {
            return;
        }
        Destroy(curHUD);
        foreach (Transform child in panelToAttachStuff.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        foreach (DropdownObject code in storedCodes)
        {
            codes.Add(code);
            GameObject temp = createReplacementDropdown();
            //tempDd.GetComponent<Dropdown>().value = temp.GetComponent<Dropdown>().value;
            int tempFirst = code.getCurrCode().IndexOf('(');
            int tempSecond = code.getCurrCode().IndexOf(')');
            temp.transform.GetChild(0).GetComponent<Text>().text = code.getCurrCode().Substring(0, tempFirst + 1) + code.getCurrCode().Substring(tempSecond);//Changing text
            Debug.Log(temp.GetComponent<Dropdown>().value);
            if (code.getCurrCode().Substring(0, 6).Equals("stopAt"))
            {
                GameObject tempText = createText();
                int first = code.getCurrCode().IndexOf('(');
                int second = code.getCurrCode().IndexOf(')');
                if(curRange != null)
                {
                    Debug.Log("Still here");
                }
                if(curRange == null)
                {
                    curRange = Instantiate(Resources.Load("Range"), GameObject.Find(currPlayer).transform.position, GameObject.Find(currPlayer).transform.rotation) as GameObject;
                    curRange.transform.SetParent(GameObject.Find(currPlayer).transform);
                    curRange.transform.SetSiblingIndex(1);
                    Debug.Log("New");
                }
                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                int currVal = int.Parse(code.getCurrCode().Substring(first + 1, second - first - 1));
                GameObject player = GameObject.Find(currPlayer);
                if(curRange != null)
                {
                    player.transform.GetChild(1).transform.localScale = new Vector3(1, 1, 1) * (int)(currVal);
                }
                
                Debug.Log(currVal);
                tempText.GetComponent<Text>().text = "Stop Dist: " + currVal;
                GameObject tempSlider = createSlider();
                tempSlider.GetComponent<Slider>().value = (float)(currVal) / 20;
                temp.GetComponent<DropdownProperties>().setText(tempText);
                temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(temp, tempSlider); });
            }
            if (code.getCurrCode().Substring(0, 6).Equals("setThr"))
            {
                GameObject tempText = createText();
                int first = code.getCurrCode().IndexOf('(');
                int second = code.getCurrCode().IndexOf(')');

                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                int currVal = int.Parse(code.getCurrCode().Substring(first + 1, second - first - 1));
                Debug.Log(currVal);
                tempText.GetComponent<Text>().text = "Threshold Hp: " + currVal;
                GameObject tempSlider = createSlider();
                tempSlider.GetComponent<Slider>().value = (float)(currVal) / 100;
                temp.GetComponent<DropdownProperties>().setText(tempText);
                temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedThresholdHp(temp, tempSlider); });
            }
            if (code.getCurrCode().Substring(0, 6).Equals("setTar"))
            {
                GameObject tempTarget = createDropdownTarget();
                tempTarget.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnTargetChange(temp, tempTarget); });
                int first = code.getCurrCode().IndexOf("\"");
                int second = code.getCurrCode().IndexOf("\"", first + 1);

                Debug.Log(code.getCurrCode().Substring(first + 1, second - first - 1));
                string currVal = code.getCurrCode().Substring(first + 1, second - first - 1);
                tempTarget.transform.GetChild(0).GetComponent<Text>().text = currVal;
                temp.GetComponent<DropdownProperties>().setTarget(tempTarget);
                //Debug.Log("I'MMMMMMMMMMMMMMMMMMMMMMMMMMMM INNNNNNNNNNNNNNNNNNNNNNNNNNNNN");
                //GameObject tempSlider = createSlider();
                //tempSlider.GetComponent<Slider>().value = (float)(currVal) / 20;
                //temp.GetComponent<DropdownProperties>().setText(tempText);
                //temp.GetComponent<DropdownProperties>().setSlider(tempSlider);
                //tempSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { sliderOnValueChangedStopAt(temp, tempSlider); });
            }
            temp.GetComponent<DropdownProperties>().setOldCode(code);
            temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { newOnValueChanged(temp); });//Setting what dropdown does when valueChanged
        }
        temp = createFirstDropdown();
        temp.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { OnValueChanged(); });//Setting what button does when clicked
    }
}
