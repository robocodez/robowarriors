﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelScript : MonoBehaviour {

    public string gamelevel;

    public void newGame()
    {   
        UnityEngine.SceneManagement.SceneManager.LoadScene(gamelevel);
        SavedData.resetData();
    }
}
