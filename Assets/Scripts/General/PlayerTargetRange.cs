﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetRange : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Entered");
        if (collision.gameObject.transform.parent != null && collision.gameObject.transform.parent.gameObject.tag == "Enemy")
        {
            GameManager.addTarget(collision.gameObject.transform.parent.gameObject);
            Debug.Log("Added");
            
        }
    }
}
