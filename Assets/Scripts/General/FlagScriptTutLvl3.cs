﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagScriptTutLvl3 : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !collision.gameObject.GetComponent<PlayerTank>().getFlagStatus())
        {
            collision.gameObject.GetComponent<Player1Script>().getRigidBody().velocity = Vector2.zero;
            collision.gameObject.GetComponent<PlayerTank>().updateFlagStatus(true);
            Destroy(this.gameObject);
        }
    }
}
