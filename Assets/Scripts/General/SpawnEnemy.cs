﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {

    private int count;
    public string enemyName;
	// Use this for initialization
	void Start () {
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (ObjectStorage.getEnemyFlags() == null && count != 1)
        {
            Instantiate(Resources.Load(enemyName), transform.position, transform.rotation);
            count++;
        }
	}
}
