﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;
using System.Linq;
using UnityEngine.UI;

public class Player1Script : MonoBehaviour
{
    //AI codes
    List<DropdownObject> codes;
    Script LuaScript;
    private static GameObject player;
    public int playerNum;
    public Text text;

    //holds Gameobjects of the missile's target and the closest GameObject to the player
    private static GameObject closestTarget;

    //holds vector values of the distance between distances of targets and the player's current position
    private static Vector3 distanceDifference;


    //decimal values that hold distances of the player to GameObjects
    private static float currentDistance;

    //for Chase function
    private CircleCollider2D col2D;
    public GameObject targ;
    public float speed;
    bool borderCheck;
    private Rigidbody2D rigidbody;
    private int activeRange;

    //for Shoot function
    public GameObject Bullet_Emitter;
    public GameObject Bullet;
    public float Bullet_Forward_Force;
    private AudioSource source;
    public AudioClip shootSound;
    private bool shot;

    // Use this for initialization
    void Start()
    {
        //col2D = this.GetComponent<CircleCollider2D>();
        //col2D.radius = 10;
        rigidbody = this.GetComponent<Rigidbody2D>();
        //codes = AIConfig.codes;
        LuaScript = new Script();
        UserData.RegisterType<Player1Script>();
        //instaniates the hazards' array
        LuaScript.Globals["Chase"] = (Func<int>)Chase;
        LuaScript.Globals["moveTo"] = (Func<int>)moveTo;
        LuaScript.Globals["Escape"] = (Func<int>)Escape;
        LuaScript.Globals["Shoot"] = (Func<int>)Shoot;
        LuaScript.Globals["setThresholdAt"] = (Func<int, int>)setThresholdAt;
        LuaScript.Globals["enemyFlags"] = (Func<int>)Goal;
        LuaScript.Globals["returnSP"] = (Func<int>)returnSP;
        LuaScript.Globals["stopAt"] = (Func<int, int>)stopAt;
        LuaScript.Globals["guardFlags"] = (Func<int>)guardFlags;
        LuaScript.Globals["setTarget"] = (Func<string, int>)setTarget;
        player = this.gameObject;
        codes = NewAIConfig.getCodes(playerNum);
        source = GetComponent<AudioSource>();
        shootSound = Resources.Load("Tank_Firing") as AudioClip;
        rigidbody.isKinematic = false;
        //rigidbody.detectCollisions = true;
        /*if (codes != null)
        {
            Debug.Log(playerNum);
        }
        if(codes == null)
        {
            Debug.Log(playerNum + " is null");
        }*/
        shot = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        rigidbody.freezeRotation = true;
        if (GameManager.getPos(playerNum) == 0)
        {
            ObjectStorage.resetTarget(playerNum);
        }
        if (GameManager.getPos(playerNum) < codes.Count())
        {
            LuaScript.DoString((codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode());
        }
        else
        {
            rigidbody.velocity = Vector2.zero;
            GameManager.resetPos(playerNum);
        }
        //Debug.Log(GameManager.getPos(playerNum));
        //rigidbody.velocity = Vector2.zero;
        /*catch(Exception e)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }*/
    }

    public Rigidbody2D getRigidBody()
    {
        return rigidbody;
    }

    void MoveDown()
    {
        transform.Translate(Vector2.up * 0);
        transform.Translate(Vector2.down * Time.deltaTime);
    }

    void MoveUp()
    {
        transform.Translate(Vector2.down * 0);
        transform.Translate(Vector2.up * Time.deltaTime);
    }


    public int Chase()
    {
        //Initialisation of variables
        List<GameObject> enemyArray = GameManager.getEnemies();
        GameObject curr = this.gameObject;
        //ObjectStorage.getPlayer1();
        GameObject targ = getClosest(curr, enemyArray);
        /*if (ObjectStorage.getChasee(playerNum) == null)
        {
            targ = getClosest(curr, enemyArray);
        }*/
        
        //GameObject targ = OnTriggerEnter2D(col2D);
        //Condition for when there is a target and when there is no target
        if (targ != null)
        {   
            
            //Get distance to player
            Vector2 playerDist = (targ.transform.position - curr.transform.position);
            //Debug.Log(playerDist.magnitude);

            Vector2 velocity = Vector2.zero;
            //Debug.Log("Player DETECTED");
            //follow player
            if (playerDist.magnitude > ObjectStorage.getDist(playerNum))
            {
                rigidbody.velocity = Vector2.zero;
                velocity += playerDist.normalized * speed;
                text.GetComponent<Text>().text = (codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode();
            }
            else
            {
                GameManager.increasePos(playerNum);
                //rigidbody.velocity = velocity;
                return 1;
            }
            rigidbody.velocity = velocity;
            float angle1 = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
            Quaternion q1 = Quaternion.AngleAxis(angle1, Vector3.forward);
            curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q1, Time.deltaTime * 6);

            //player movement towards the target


            //transform.right = (playerDist).normalized;

            //player direction, facing towards the target
            GameManager.resetPos(playerNum);
            
        }
        else
        {
            GameManager.increasePos(playerNum);
            return 1;
        }
        return 1;

    }

    public int Shoot()
    {
        List<GameObject> enemyArray = GameManager.getEnemies();
        GameObject curr = this.gameObject;
        GameObject Bullet_Emitter = (curr.transform.GetChild(1)).gameObject;
        Debug.Log(enemyArray.Count);
        /*Vector2 vectorDirection0 = targ.transform.position - curr0.transform.position;
        Vector2 vectorDirection1 = targ.transform.position - curr1.transform.position;
        Vector2 vectorDirection2 = targ.transform.position - curr2.transform.position;
        Vector2 vectorDirection3 = targ.transform.position - curr3.transform.position;
        Vector2 vectorDirection4 = targ.transform.position - curr4.transform.position;

        float playerDist0 = Vector2.Distance(targ.transform.position, curr0.transform.position);
        float playerDist1 = Vector2.Distance(targ.transform.position, curr1.transform.position);
        float playerDist2 = Vector2.Distance(targ.transform.position, curr2.transform.position);
        float playerDist3 = Vector2.Distance(targ.transform.position, curr3.transform.position);
        float playerDist4 = Vector2.Distance(targ.transform.position, curr4.transform.position);*/

        //if(enemyArray.Count != 0)
        //{
            
        if (individualFire(curr, Bullet_Emitter, GameManager.getNextFire(playerNum)))
        {
            rigidbody.velocity = Vector2.zero;
            if(shot == true)
            {
                GameManager.resetNextFire(playerNum);
                shot = false;
            }    
            text.GetComponent<Text>().text = (codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode();
            GameManager.resetPos(playerNum);
        }
            
            
        //}
        else
        {
            GameManager.increasePos(playerNum);
        }
        return 1;

    }

    private Boolean individualFire(GameObject curr, GameObject be, float nf)
    {
        //Initialisation of variables
        List<GameObject> enemyArray = GameManager.getEnemies();
        float Bullet_Forward_Force = 1000;
        Boolean check = false;
        GameObject closestTarget;
        Vector3 playerPosition = curr.transform.position;

        if (enemyArray.Count != 0)
        {
            closestTarget = getClosest(curr, enemyArray);
            //player faces target to shoot
            Vector2 vd = closestTarget.transform.position - playerPosition;
            float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            //tells the bullet to be shot forward by a set amount of Bullet_Forward_Force
            if (vd.magnitude <= 6.6f)
            {
                
                curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);
                if (nf >= 10)
                {
                    //instantiate and create a bullet object
                    GameObject temp_bullet;
                    temp_bullet = Instantiate(Resources.Load("Bullet 1"), be.transform.position, be.transform.rotation) as GameObject;


                    // correct rotation of bullet shot
                    // temp_bullet.transform.Rotate(Vector2.left * 90);

                    //retrieve the rigidbody component from the instantiated bullet and control it
                    Rigidbody2D temp_rigidbody;
                    temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
                    temp_rigidbody.AddForce(curr.transform.right * Bullet_Forward_Force);

                    //ensures the bullet disappears after a set period
                    Destroy(temp_bullet, 1.0f);
                    shot = true;
                    Debug.Log("Shot");
                }
                
                check = true;
            }
        }
        return check;
    }

    public GameObject getClosest(GameObject curr, List<GameObject> list)
    {
        Vector3 playerPosition = curr.transform.position;
        GameObject closestTarget = null;
        float oldDistance = Mathf.Infinity;
        Debug.Log(list.Count);
        foreach (GameObject go in list)
        {   
            if(go == null)
            {
                continue;
            }
            //the first step in the for loop is to find the distance between the player and the current GameObject we are solving for in the Array
            distanceDifference = go.transform.position - playerPosition;

            //we set the float currentDistance to the square magnitude of the distanceDifference.  Since distanceDifference is a Vector 3, sqrMagnitude helps us find the float value
            currentDistance = distanceDifference.sqrMagnitude;

            /*now that we have the currentDistance of the Hazard[i] we need to check three conditions
            1st condition - We need to compare the oldDistance of the last Hazard in the array to the currentDistance of the current Hazard in the array we are solving for
            2nd condition - We need to acces the current Hazard's IsTargetted componet.  It has a bool variable called isTargeted.  If the hazard hasn't been targeted its all good.
            3rd condition - We don't want missiles to launch backwards from the ship.  So we compare the player's position to the hazard's position and make sure the hazards are in about 5 units in front.
            if all 3 of the conditions are true....We have found our new closest Target.*/

            if (currentDistance < oldDistance)
            //go.GetComponent<IsTargeted>().isTargeted == false)
            //&& playerPosition.z < go.transform.position.z - 5)
            {

                //this sets the closestTarget to the current Hazard we are solving for in our array
                closestTarget = go;

                //this sets the oldDistance to the currentDistance for the next cycle through the loop.  If the next Hazard in the array is closer, this will let us know
                oldDistance = currentDistance;

                //when we find the closest target we need to change the Hazard's IsTargetted componet to isTargeted true.  This keeps missiles from targetting already targetted Hazards.
                go.GetComponent<IsTargeted>().isTargeted = true;
            }
        }
        return closestTarget;
    }

    public GameObject getFurthest(GameObject curr, List<GameObject> list)
    {
        Vector3 playerPosition = curr.transform.position;
        GameObject furthestTarget = null;
        float oldDistance = 0;
        foreach (GameObject go in list)
        {
            //the first step in the for loop is to find the distance between the player and the current GameObject we are solving for in the Array
            distanceDifference = go.transform.position - playerPosition;

            //we set the float currentDistance to the square magnitude of the distanceDifference.  Since distanceDifference is a Vector 3, sqrMagnitude helps us find the float value
            currentDistance = distanceDifference.sqrMagnitude;

            /*now that we have the currentDistance of the Hazard[i] we need to check three conditions
            1st condition - We need to compare the oldDistance of the last Hazard in the array to the currentDistance of the current Hazard in the array we are solving for
            2nd condition - We need to acces the current Hazard's IsTargetted componet.  It has a bool variable called isTargeted.  If the hazard hasn't been targeted its all good.
            3rd condition - We don't want missiles to launch backwards from the ship.  So we compare the player's position to the hazard's position and make sure the hazards are in about 5 units in front.
            if all 3 of the conditions are true....We have found our new closest Target.*/

            if (currentDistance > oldDistance)
            //go.GetComponent<IsTargeted>().isTargeted == false)
            //&& playerPosition.z < go.transform.position.z - 5)
            {

                //this sets the closestTarget to the current Hazard we are solving for in our array
                furthestTarget = go;

                //this sets the oldDistance to the currentDistance for the next cycle through the loop.  If the next Hazard in the array is closer, this will let us know
                oldDistance = currentDistance;

                //when we find the closest target we need to change the Hazard's IsTargetted componet to isTargeted true.  This keeps missiles from targetting already targetted Hazards.
                go.GetComponent<IsTargeted>().isTargeted = true;
            }
        }
        return furthestTarget;
    }


    public int Goal()
    {
        
        GameObject curr = this.gameObject;
        //GameObject curr = this.gameObject;
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();

        GameObject goal = ObjectStorage.getEnemyFlags();
        if (goal == null)
        {
            GameManager.increasePos(playerNum);
            //rigidbody.velocity = Vector2.zero;
            return 1;
        }
        rigidbody.velocity = Vector2.zero;

        //Get distance to player
        Vector2 playerDist = (goal.transform.position - curr.transform.position);
        //Debug.Log(playerDist.magnitude);

        Vector2 velocity = Vector2.zero;

        //Debug.Log("Player DETECTED");
        //follow player

        //player movement towards the target
        velocity += playerDist.normalized * speed;
        rigidbody.velocity = velocity;

        //transform.right = (playerDist).normalized;

        //player direction, facing towards the target
        float angle = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);
        text.GetComponent<Text>().text = (codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode();
        GameManager.resetPos(playerNum);
        return 1;
    }

    public void hello()
    {
        Debug.Log("Hello");
    }
    public void Move()
    {
        //Initialisation of variables
        GameObject curr = GameObject.Find("Player1");
 
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();

        Vector2 velocity = Vector2.zero;
        velocity += Vector2.right * speed;
        rigidbody.velocity = velocity;
    }

    public int stopAt(int num)
    {
        ObjectStorage.setDist(num, playerNum);
        GameManager.increasePos(playerNum);
        return 1;
    }

    public int returnSP()
    {
        GameObject curr = this.gameObject;
        //GameObject curr = this.gameObject;
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();
        Debug.Log("Inside returnSp");
        GameObject goal = ObjectStorage.getPlayerSP();
        if (!this.gameObject.GetComponent<PlayerTank>().getFlagStatus() && this.gameObject.GetComponent<PlayerTank>().getHp() == this.gameObject.GetComponent<PlayerTank>().getMaxHp())
        {
            //rigidbody.velocity = Vector2.zero;
            Debug.Log("True");
            GameManager.increasePos(playerNum);
            return 1;
        }
        rigidbody.velocity = Vector2.zero;

        //Get distance to player
        Vector2 playerDist = (goal.transform.position - curr.transform.position);
        //Debug.Log(playerDist.magnitude);

        Vector2 velocity = Vector2.zero;

        //Debug.Log("Player DETECTED");
        //follow player

        //player movement towards the target
        velocity += playerDist.normalized * speed;
        rigidbody.velocity = velocity;

        //transform.right = (playerDist).normalized;

        //player direction, facing towards the target
        float angle = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);
        text.GetComponent<Text>().text = (codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode();
        GameManager.resetPos(playerNum);
        return 1;
    }

    public int guardFlags()
    {
        Debug.Log("Inside");
        GameObject curr = this.gameObject;
        //GameObject curr = this.gameObject;
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();
       
        GameObject goal = ObjectStorage.getPlayerFlags();
        if (goal == null)
        {
            Debug.Log("Null goals");
            rigidbody.velocity = Vector2.zero;
            GameManager.increasePos(playerNum);
            return 1;
        }
        rigidbody.velocity = Vector2.zero;

        //Get distance to player
        Vector2 playerDist = (goal.transform.position - curr.transform.position);
        if (playerDist.magnitude < ObjectStorage.getDist(playerNum))
        {
            Debug.Log("Too close");
            //rigidbody.velocity = Vector2.zero;
            GameManager.increasePos(playerNum);
            return 1;
        }
        Debug.Log(playerDist.magnitude);

        Vector2 velocity = Vector2.zero;

        //Debug.Log("Player DETECTED");
        //follow player

        //player movement towards the target
        velocity += playerDist.normalized * speed;
        rigidbody.velocity = velocity;

        //transform.right = (playerDist).normalized;

        //player direction, facing towards the target
        float angle = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);
        return 1;
    }

    public int Escape()
    {
        GameObject curr = this.gameObject;
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();

        GameObject goal = ObjectStorage.getPlayerSP();
        int currHp = curr.GetComponent<PlayerTank>().getHp();
        Debug.Log("Escaping");
        if(ObjectStorage.getThresholdHp(playerNum) > curr.GetComponent<PlayerTank>().getMaxHp())
        {
            return 1;
        }

        if(currHp < ObjectStorage.getThresholdHp(playerNum))
        {
            rigidbody.velocity = Vector2.zero;
            //Get distance to player
            Vector2 playerDist = (goal.transform.position - curr.transform.position);

            Vector2 velocity = Vector2.zero;

            //Debug.Log("Player DETECTED");
            //follow player
            //player movement towards the target
            velocity += playerDist.normalized * speed;
            rigidbody.velocity = velocity;

            //transform.right = (playerDist).normalized;

            //player direction, facing towards the target
            float angle = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);
        }
        else
        {
            GameManager.increasePos(playerNum);
            
        }
        return 1;
    }

    public int setThresholdAt(int num)
    {
        ObjectStorage.setThresholdHp(num, playerNum);
        GameManager.increasePos(playerNum);
        return 1;
    }

    public int setTarget(string go)
    {
        ObjectStorage.setChasee(playerNum, go);
        GameManager.increasePos(playerNum);
        return 1;
    }

    public int moveTo()
    {
        GameObject curr = this.gameObject;
        //ObjectStorage.getPlayer1();
        GameObject targ = ObjectStorage.getChasee(playerNum);
        if (codes.ElementAt(GameManager.getPos(playerNum)).getChecked())
        {
            Debug.Log("problem1");
            GameManager.increasePos(playerNum);
            return 1;
        }
        //GameObject targ = OnTriggerEnter2D(col2D);
        //Condition for when there is a target and when there is no target
        if (targ != null)
        {

            //Get distance to player
            Vector2 playerDist = (targ.transform.position - curr.transform.position);
            //Debug.Log(playerDist.magnitude);

            Vector2 velocity = Vector2.zero;
            //Debug.Log("Player DETECTED");
            //follow player
            if (playerDist.magnitude > ObjectStorage.getDist(playerNum))
            {
                rigidbody.velocity = Vector2.zero;
                velocity += playerDist.normalized * speed;
                text.GetComponent<Text>().text = (codes.ElementAt(GameManager.getPos(playerNum))).getCurrCode();
            }
            else
            {
                codes.ElementAt(GameManager.getPos(playerNum)).isChecked();
                GameManager.increasePos(playerNum);
                Debug.Log("problem2");
                //rigidbody.velocity = velocity;
                return 1;
            }
            rigidbody.velocity = velocity;
            float angle1 = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
            Quaternion q1 = Quaternion.AngleAxis(angle1, Vector3.forward);
            curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q1, Time.deltaTime * 6);

            //player movement towards the target


            //transform.right = (playerDist).normalized;

            //player direction, facing towards the target
            GameManager.resetPos(playerNum);

        }
        else
        {
            GameManager.increasePos(playerNum);
            return 1;
        }
        return 1;
    }
}