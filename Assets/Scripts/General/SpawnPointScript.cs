﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnPointScript : MonoBehaviour {

    public Button nextLvlBtn;
    private int score;
	// Use this for initialization
	void Start () {
        if(nextLvlBtn != null)
        {
            nextLvlBtn.gameObject.SetActive(false);
        }
        score = 0;
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("collided");
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerTank>().resetHp();
            Debug.Log("Heal");
            addScore();
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            if (collision.gameObject.GetComponent<PlayerTank>().getFlagStatus())
            {
                collision.gameObject.GetComponent<PlayerTank>().updateFlagStatus(false);
                if(nextLvlBtn != null)
                {
                    nextLvlBtn.gameObject.SetActive(true);
                }
                
            }
        }
    }

    private void addScore()
    {
        score++;
        Debug.Log("Added");
    }

    public int getScore()
    {
        return score;
    }

}
