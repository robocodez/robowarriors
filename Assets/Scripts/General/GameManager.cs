﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject tank;
    public int maxNum;
    public int currNum;
    public GameObject SP;
    public float spawnRate = 0.5f;
    private float nextSpawn = 0.0f;
    private float fireRate = 0.2f;
    
    private static float nextFire1 = 0.0f;
    private static float nextFire2 = 0.0f;
    private static float nextFire3 = 0.0f;
    private static float nextFire4 = 0.0f;
    private static float nextFire5 = 0.0f;
    // Use this for initialization

    private static int pos1;
    private static int pos2;
    private static int pos3;
    private static int pos4;
    private static int pos5;

    //declares the array that holds all the enemy currently on screen
    private static List<GameObject> enemyArray;
    private static List<GameObject> playerArray;

    private bool isPaused;

    public string currLvl;

    public GameObject menu;

    void Start () {
        currNum = 5;
        enemyArray = new List<GameObject>();
        playerArray = new List<GameObject>();
        
        pos1 = 0;
        pos2 = 0;
        pos3 = 0;
        pos4 = 0;
        pos5 = 0;

        isPaused = false;
        if (menu != null)
        {
            menu.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (currNum < 5 && nextSpawn == 100.0f)
        {   
            GameObject newTank;
            newTank = Instantiate(tank, SP.transform.position, SP.transform.rotation) as GameObject;
            currNum += 1;
            nextSpawn = 0;
        }
        else
        {
            nextSpawn += spawnRate;
        }
        
        nextFire1 += fireRate;
        nextFire2 += fireRate;
        nextFire3 += fireRate;
        nextFire4 += fireRate;
        nextFire5 += fireRate;
        if (isPaused)
        {
            Time.timeScale = 0; ;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public static void resetNextFire(int playerNum)
    {
        if (playerNum == 1)
        {
            nextFire1 = 0;
        }
        if (playerNum == 2)
        {
            nextFire2 = 0;
        }
        if (playerNum == 3)
        {
            nextFire3 = 0;
        }
        if (playerNum == 4)
        {
            nextFire4 = 0;
        }
        if (playerNum == 5)
        {
            nextFire5 = 0;
        }
    }

    /*public static void resetNextFire1()
    {
        nextFire1 = 0;
    }

    public static void resetNextFire2()
    {
        nextFire2 = 0;
    }

    public static void resetNextFire3()
    {
        nextFire3 = 0;
    }

    public static void resetNextFire4()
    {
        nextFire4 = 0;
    }*/

    public static float getNextFire(int playerNum)
    {
        float nf = 0.0f;
        
        if (playerNum == 1)
        {
            nf = nextFire1;
        }
        if (playerNum == 2)
        {
            nf = nextFire2;
        }
        if (playerNum == 3)
        {
            nf = nextFire3;
        }
        if (playerNum == 4)
        {
            nf = nextFire4;
        }
        if (playerNum == 5)
        {
            nf = nextFire5;
        }
        return nf;
    }

    /*public static float getNextFire1()
    {
        return nextFire1;
    }

    public static float getNextFire2()
    {
        return nextFire2;
    }

    public static float getNextFire3()
    {
        return nextFire3;
    }

    public static float getNextFire4()
    {
        return nextFire4;
    }*/

    public static void addTarget(GameObject go)
    {
        if (!enemyArray.Contains(go))
        {
            enemyArray.Add(go);
            
        }
    }

    public static void removeTarget(GameObject go)
    {
        if (enemyArray.Contains(go))
        {
            enemyArray.Remove(go);
        }
    }

    public static List<GameObject> getEnemies()
    {
        return enemyArray;
    }

    public static void addPlayerTarget(GameObject go)
    {
        if (!playerArray.Contains(go))
        {
            playerArray.Add(go);
        }
    }

    public static void removePlayerTarget(GameObject go)
    {
        if (playerArray.Contains(go))
        {   
            playerArray.Remove(go);
        }
    }

    public static List<GameObject> getPlayers()
    {
        return playerArray;
    }

    //getting position and changing position of the line of codes
    public static int getPos(int playerNum)
    {
        int pos = -1;
        
        if (playerNum == 1)
        {
            pos = pos1;
        }
        if (playerNum == 2)
        {
            pos = pos2;
        }
        if (playerNum == 3)
        {
            pos = pos3;
        }
        if (playerNum == 4)
        {
            pos = pos4;
        }
        if (playerNum == 5)
        {
            pos = pos5;
        }
        return pos;
    }

    public static int getPos0()
    {
        return pos1;
    }

    /*public static int getPos2()
    {
        return pos2;
    }

    public static int getPos3()
    {
        return pos3;
    }

    public static int getPos4()
    {
        return pos4;
    }*/

    public static void increasePos(int playerNum)
    {
        if (playerNum == 1)
        {
            pos1 += 1;
        }
        if (playerNum == 2)
        {
            pos2 += 1;
        }
        if (playerNum == 3)
        {
            pos3 += 1;
        }
        if (playerNum == 4)
        {
            pos4 += 1;
        }
        if (playerNum == 5)
        {
            pos5 += 1;
        }
    }

    public static void increasePos0()
    {
        pos1 += 1;
    }

    /*public static void increasePos2()
    {
        pos2 += 1;
    }

    public static void increasePos3()
    {
        pos3 += 1;
    }

    public static void increasePos4()
    {
        pos4 += 1;
    }*/

    public static void resetPos(int playerNum)
    {
        if (playerNum == 1)
        {
            pos1 = 0;
        }
        if (playerNum == 2)
        {
            pos2 = 0;
        }
        if (playerNum == 3)
        {
            pos3 = 0;
        }
        if (playerNum == 4)
        {
            pos4 = 0;
        }
        if (playerNum == 5)
        {
            pos5 = 0;
        }
    }

    public static void resetPos0()
    {
        pos1 = 0;
    }

    /*public static void resetPos2()
    {
        pos2 = 0;
    }

    public static void resetPos3()
    {
        pos3 = 0;
    }

    public static void resetPos4()
    {
        pos4 = 0;
    }*/

    public void Pause()
    {
        if (isPaused)
        {
            isPaused = false;
            GameObject.Find("PauseBtn").GetComponentInChildren<Text>().text = "Pause";
            menu.SetActive(false);
            
        }
        else
        {
            isPaused = true;
            GameObject.Find("PauseBtn").GetComponentInChildren<Text>().text = "Continue";
            menu.SetActive(true);
        }
    }

    public void Retry()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(currLvl);
        isPaused = false;
    }

    public void Quit()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu3D");
        isPaused = false;
        SavedData.resetData();
    }

    public static void resetEnemyList()
    {
        enemyArray = new List<GameObject>();
    }

    public void showHp()
    {
        ObjectStorage.setTrue();
    }
}


