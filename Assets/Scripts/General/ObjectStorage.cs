﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStorage : MonoBehaviour{

    private static GameObject player0;
    private static GameObject player1;
    private static GameObject player2;
    private static GameObject player3;
    private static GameObject player4;


    private static int dist1;
    private static int dist2;
    private static int dist3;
    private static int dist4;
    private static int dist5;

    private static GameObject enemyFlag;
    private static GameObject playerFlag;


    private static int currHp1;
    private static int currHp2;
    private static int currHp3;
    private static int currHp4;
    private static int currHp5;

    private static GameObject playerSP;
    private static GameObject enemySP;

    public static List<string> list;

    private static GameObject chasee1;
    private static GameObject chasee2;
    private static GameObject chasee3;
    private static GameObject chasee4;
    private static GameObject chasee5;

    public static bool showHp;


    // Use this for initialization

    private void Awake()
    {
        list = new List<string> { "Chase();", "Shoot();", "Goal();", "Move();", "returnSP();", "guardFlags();", "Escape();", "stopAt();" };
        enemyFlag = GameObject.Find("Enemy Flags");
        showHp = false;
    }
    void Start () {
        /*player0 = GameObject.Find("Player");
        player1 = GameObject.Find("Player1");
        player2 = GameObject.Find("Player2");
        player3 = GameObject.Find("Player3");
        player4 = GameObject.Find("Player4");*/

        
        dist1 = 5;
        dist2 = 5;
        dist3 = 5;
        dist4 = 5;
        dist5 = 5;

        enemyFlag = GameObject.Find("Enemy Flags");
        playerFlag = GameObject.Find("Player Flags");

        
        currHp1 = 50;
        currHp2 = 50;
        currHp3 = 50;
        currHp4 = 50;
        currHp5 = 50;

        playerSP = GameObject.Find("SpawnPoint"); ;
        enemySP = GameObject.Find("EnemySpawnPoint"); ;

        chasee1 = null;
        chasee2 = null;
        chasee3 = null;
        chasee4 = null;
        chasee5 = null;

        
    }
	
	// Update is called once per frame
	void Update () {
        

	}

    public static GameObject getPlayer0()
    {
        return player0;
    }

    public static GameObject getPlayer1()
    {
        return player1;
    }

    public static GameObject getPlayer2()
    {
        return player2;
    }

    public static GameObject getPlayer3()
    {
        return player3;
    }

    public static GameObject getPlayer4()
    {
        return player4;
    }

    public static void setDist(int num, int currPlayer)
    {
        if (currPlayer == 1)
        {
            dist1 = 2 + num;
        }
        if (currPlayer == 2)
        {
            dist2 = 2 + num;
        }
        if (currPlayer == 3)
        {
            dist3 = 2 + num;
        }
        if (currPlayer == 4)
        {
            dist4 = 2 + num;
        }
        if (currPlayer == 5)
        {
            dist5 = 2 + num;
        }
    }

    /*public static void setDist1(int num)
    {
        dist1 = num;
    }

    public static void setDist2(int num)
    {
        dist2 = num;
    }

    public static void setDist3(int num)
    {
        dist3 = num;
    }

    public static void setDist4(int num)
    {
        dist4 = num;
    }*/

    public static int getDist(int playerNum)
    {
        int currMaxDist = 0;
        if (playerNum == 1)
        {
            currMaxDist = dist1;
        }
        if (playerNum == 2)
        {
            currMaxDist = dist2;
        }
        if (playerNum == 3)
        {
            currMaxDist = dist3;
        }
        if (playerNum == 4)
        {
            currMaxDist = dist4;
        }
        if (playerNum == 5)
        {
            currMaxDist = dist5;
        }
        return currMaxDist;
    }

    /*public static int getDist1()
    {
        return dist1;
    }

    public static int getDist2()
    {
        return dist2;
    }

    public static int getDist3()
    {
        return dist3;
    }

    public static int getDist4()
    {
        return dist4;
    }

    public static int getDist5()
    {
        return dist5;
    }*/

    public static GameObject getPlayerFlags()
    {
        return playerFlag;
    }

    public static GameObject getEnemyFlags()
    {
        return enemyFlag;
    }

    public static int getThresholdHp(int playerNum)
    {
        int currMinHp = 0;
        if (playerNum == 1)
        {
            currMinHp = currHp1;
        }
        if (playerNum == 2)
        {
            currMinHp = currHp2;
        }
        if (playerNum == 3)
        {
            currMinHp = currHp3;
        }
        if (playerNum == 4)
        {
            currMinHp = currHp4;
        }
        if (playerNum == 5)
        {
            currMinHp = currHp5;
        }
        return currMinHp;
    }

    /*public static int getThresholdHp()
    {
        return currHp1;
    }

    public static int getThresholdHp2()
    {
        return currHp2;
    }

    public static int getThresholdHp3()
    {
        return currHp3;
    }

    public static int getThresholdHp4()
    {
        return currHp4;
    }

    public static int setThresholdHp0()
    {
        return currHp4;
    }*/

    public static void setThresholdHp(int num, int playerNum)
    {
        if (playerNum == 1)
        {
            currHp1 = num;
        }
        if (playerNum == 2)
        {
            currHp2 = num;
        }
        if (playerNum == 3)
        {
            currHp3 = num;
        }
        if (playerNum == 4)
        {
            currHp4 = num;
        }
        if (playerNum == 5)
        {
            currHp5 = num;
        }
    }

    /*public static void setThresholdHp2(int num)
    {
        currHp2 = num;
    }

    public static void setThresholdHp3(int num)
    {
        currHp3 = num;
    }

    public static void setThresholdHp4(int num)
    {
        currHp4 = num;
    }*/

    public static GameObject getPlayerSP()
    {
        return playerSP;
    }

    public static GameObject getEnemySP()
    {
        return enemySP;
    }

    public static List<string> getList()
    {
        return list;
    }

    public static GameObject getChasee(int playerNum)
    {
        GameObject currTarget = null;
        if (playerNum == 1)
        {
            currTarget = chasee1;
        }
        if (playerNum == 2)
        {
            currTarget = chasee2;
        }
        if (playerNum == 3)
        {
            currTarget = chasee3;
        }
        if (playerNum == 4)
        {
            currTarget = chasee4;
        }
        if (playerNum == 5)
        {
            currTarget = chasee5;
        }
        return currTarget;
    }

    public static void setChasee(int playerNum, string go)
    {
        if (playerNum == 1)
        {
            chasee1 = GameObject.Find(go);
        }
        if (playerNum == 2)
        {
            chasee2 = GameObject.Find(go);
        }
        if (playerNum == 3)
        {
            chasee3 = GameObject.Find(go);
        }
        if (playerNum == 4)
        {
            chasee4 = GameObject.Find(go);
        }
        if (playerNum == 5)
        {
            chasee5 = GameObject.Find(go);
        }
    }

    public static void resetTarget(int playerNum)
    {
        if (playerNum == 1)
        {
            chasee1 = null;
        }
        if (playerNum == 2)
        {
            chasee2 = null;
        }
        if (playerNum == 3)
        {
            chasee3 = null;
        }
        if (playerNum == 4)
        {
            chasee4 = null;
        }
        if (playerNum == 5)
        {
            chasee5 = null;
        }
    }

    public static void setTrue()
    {
        showHp = true;
    }

    public static bool getTrue()
    {
        return showHp;
    }
}
