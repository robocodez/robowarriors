﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
