﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutLvl3WinningCondition : MonoBehaviour {


    private GameObject sp;
    public Button nextLvlBtn;
    public GameObject victory;
    public GameObject effect1;
    public GameObject effect2;
    public GameObject effect3;
    // Use this for initialization
    void Start()
    {
        sp = GameObject.Find("SpawnPoint");
        nextLvlBtn.gameObject.SetActive(false);
        victory.SetActive(false);
        effect1.SetActive(false);
        effect2.SetActive(false);
        effect3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(sp.GetComponent<SpawnPointScript>().getScore());
        if (sp.GetComponent<SpawnPointScript>().getScore() > 0)
        {
            nextLvlBtn.gameObject.SetActive(true);
            victory.SetActive(true);
            effect1.SetActive(true);
            effect2.SetActive(true);
            effect3.SetActive(true);
        }
    }
}
