﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBehaviourUnderSiege : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(GameObject.Find("Enemy Flags") == null)
        {
            this.gameObject.GetComponent<EnemyTank>().AddjustCurrentHealth(-100);
        }
        this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        this.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        GameObject curr = this.gameObject;
        //GameObject curr = this.gameObject;
        Rigidbody2D rigidbody = curr.GetComponent<Rigidbody2D>();

        GameObject goal = ObjectStorage.getPlayerFlags();
        if (goal == null)
        {
            rigidbody.velocity = Vector2.zero;
            return;
        }
        rigidbody.velocity = Vector2.zero;

        //Get distance to player
        Vector2 playerDist = (goal.transform.position - curr.transform.position);
        //Debug.Log(playerDist.magnitude);

        Vector2 velocity = Vector2.zero;

        //Debug.Log("Player DETECTED");
        //follow player

        //player movement towards the target
        velocity += playerDist.normalized * 5;
        rigidbody.velocity = velocity;

        //transform.right = (playerDist).normalized;

        //player direction, facing towards the target
        float angle = Mathf.Atan2(playerDist.y, playerDist.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        curr.transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * 5);
    }
}
