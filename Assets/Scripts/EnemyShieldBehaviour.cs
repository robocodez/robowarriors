﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldBehaviour : MonoBehaviour {

    public GameObject bullet_emitter_left;
    public GameObject bullet_emitter_right;
    private GameObject curr;
    private GameObject targ;
    private Rigidbody2D rigidbody;
    private float nf = 0;
    private float speed = 3;
    // Use this for initialization
    void Start () {
        curr = this.gameObject;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Shoot(bullet_emitter_left, bullet_emitter_right);
        this.gameObject.transform.Rotate(0, 0, 60 * Time.deltaTime);
    }

    public void Shoot(GameObject be_left, GameObject be_right)
    {
        float Bullet_Forward_Force = 1000;
        //List < GameObject > playerArray = GameManager.getPlayers();
        //targ = getClosest(curr, playerArray);
        /*if(targ != null)
        {
            Vector2 vd = targ.transform.position - curr.transform.position;
            //player faces target to shoot
            float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w), q, Time.deltaTime * speed);
            
        }*/
        if (nf > 2)
        {
            //instantiate and create a bullet object
            GameObject temp_bullet_left;
            GameObject temp_bullet_right;
            temp_bullet_left = Instantiate(Resources.Load("Bullet 2"), be_left.transform.position, be_left.transform.rotation) as GameObject;
            temp_bullet_right = Instantiate(Resources.Load("Bullet 2"), be_right.transform.position, be_right.transform.rotation) as GameObject;
            //retrieve the rigidbody component from the instantiated bullet and control it
            Rigidbody2D temp_rigidbody_left;
            Rigidbody2D temp_rigidbody_right;
            temp_rigidbody_left = temp_bullet_left.GetComponent<Rigidbody2D>();
            temp_rigidbody_right = temp_bullet_right.GetComponent<Rigidbody2D>();
            temp_rigidbody_left.AddForce(transform.right * Bullet_Forward_Force);
            temp_rigidbody_right.AddForce(transform.right * Bullet_Forward_Force);
            nf = 0;
            Destroy(temp_bullet_left, 0.98f);
            Destroy(temp_bullet_right, 0.98f);
        }
        else
        {
            nf += 0.2f;
        }

    }

    public GameObject getClosest(GameObject curr, List<GameObject> list)
    {
        Vector3 playerPosition = curr.transform.position;
        GameObject closestTarget = null;
        float oldDistance = Mathf.Infinity;
        foreach (GameObject go in list)
        {
            //the first step in the for loop is to find the distance between the player and the current GameObject we are solving for in the Array
            Vector3 distanceDifference = go.transform.position - playerPosition;

            //we set the float currentDistance to the square magnitude of the distanceDifference.  Since distanceDifference is a Vector 3, sqrMagnitude helps us find the float value
            float currentDistance = distanceDifference.sqrMagnitude;

            /*now that we have the currentDistance of the Hazard[i] we need to check three conditions
            1st condition - We need to compare the oldDistance of the last Hazard in the array to the currentDistance of the current Hazard in the array we are solving for
            2nd condition - We need to acces the current Hazard's IsTargetted componet.  It has a bool variable called isTargeted.  If the hazard hasn't been targeted its all good.
            3rd condition - We don't want missiles to launch backwards from the ship.  So we compare the player's position to the hazard's position and make sure the hazards are in about 5 units in front.
            if all 3 of the conditions are true....We have found our new closest Target.*/

            if (currentDistance < oldDistance)
            //go.GetComponent<IsTargeted>().isTargeted == false)
            //&& playerPosition.z < go.transform.position.z - 5)
            {

                //this sets the closestTarget to the current Hazard we are solving for in our array
                closestTarget = go;

                //this sets the oldDistance to the currentDistance for the next cycle through the loop.  If the next Hazard in the array is closer, this will let us know
                oldDistance = currentDistance;

                //when we find the closest target we need to change the Hazard's IsTargetted componet to isTargeted true.  This keeps missiles from targetting already targetted Hazards.
                go.GetComponent<IsTargeted>().isTargeted = true;
            }
        }
        return closestTarget;
    }
}
