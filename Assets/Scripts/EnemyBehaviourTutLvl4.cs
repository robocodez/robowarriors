﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourTutLvl4 : MonoBehaviour
{

    private Vector2 velocity;
    private Rigidbody2D rigidbody;
    private float speed = 3.5f;
    private int check;
    // Use this for initialization
    void Start()
    {
        velocity = Vector2.zero;
        rigidbody = this.GetComponent<Rigidbody2D>();
        check = 0;
        rigidbody.velocity = new Vector2(3.0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Inside");
        //rigidbody.velocity = Vector2.zero;
        if (transform.position.x >= 58.3 && check == 0)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(0f, -speed);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;
        }
        else if (transform.position.y <= 26.7 && check == 1)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(-speed, 0f);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;

        }
        else if (transform.position.x <= 32.2 && check == 2)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(0f, speed);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;
        }
        else if (transform.position.y >= 39.0 && check == 3)
        {
            this.gameObject.GetComponent<EnemyTank>().AddjustCurrentHealth(-100);
            check++;
        }
    }
}
