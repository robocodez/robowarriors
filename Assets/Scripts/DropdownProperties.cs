﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropdownProperties : MonoBehaviour {

    private DropdownObject oldCode;
    private GameObject text;
    private GameObject slider;
    private GameObject targetDd;
    private GameObject _range;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public DropdownObject getOldCode()
    {
        return oldCode;
    }

    public void setOldCode(DropdownObject newCode)
    {
        oldCode = newCode;
    }

    public void setText(GameObject inText)
    {
        text = inText;
    }

    public void setSlider(GameObject inSlider)
    {
        slider = inSlider;
    }

    public GameObject getSlider()
    {
        return slider;
    }

    public GameObject getText()
    {
        return text;
    }

    public GameObject getTarget()
    {
        return targetDd;
    }

    public GameObject getRange()
    {
        return _range;
    }

    public void setRange(GameObject range)
    {
        _range = range;
    }

    public void setTarget(GameObject _targetDd)
    {
        targetDd = _targetDd;
    }
}
