﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuplicateDropdown : MonoBehaviour {

    private Dropdown dd;
    public GameObject panelToAttachStuff;
    public GameObject optionsPrefab;
    // Use this for initialization
    void Start () {
        dd = this.gameObject.GetComponent<Dropdown>();
        dd.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }
	

    void ValueChangeCheck()
    {
        GameObject options = (GameObject)Instantiate(optionsPrefab);
        options.transform.SetParent(panelToAttachStuff.transform);//Setting button parent
                                                                  //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                  //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        options.GetComponent<Dropdown>().AddOptions(ObjectStorage.getList());
        options.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text
    }
}
