﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialUIScript : MonoBehaviour {

    public GameObject nextUI;
    public GameObject panel;
	// Use this for initialization
	void Start () {
        if(nextUI != null)
        {   
            nextUI.SetActive(false);
        }
        if (nextUI == null)
        {
            Debug.Log("Empty");
        }
        Debug.Log("Running");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void done()
    {
        if(nextUI != null)
        {
            nextUI.SetActive(true);
        }
        if(panel != null)
        {
            Destroy(panel);
        }
        
        Debug.Log("Done");
        Destroy(this.gameObject);
    }
}
