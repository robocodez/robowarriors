﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {

    public static AudioSource audio;

    public static AudioClip titleTheme;
    public static AudioClip mainTheme;
	// Use this for initialization
	void Start () {
        audio = GameObject.Find("Music").GetComponent<AudioSource>();
        titleTheme = Resources.Load("Exploration Partial v2 (Robocodez Title Theme)", typeof(AudioClip)) as AudioClip;
        mainTheme = Resources.Load("Agile Manuveurs Partial v2 (Robocodez Main Theme)", typeof(AudioClip)) as AudioClip;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void changeMusic(int num)
    {
        if (num == 2 && audio.clip == titleTheme)
        {
            audio.clip = mainTheme;
            audio.Play();
        }
        if(num == 1 && audio.clip == mainTheme)
        {
            audio.clip = titleTheme;
            audio.Play();
        }
    }
}
