﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token
{
    string INTEGER = "INTEGER";
    string REAL = "REAL";
    string INTEGER_CONST = "INTEGER_CONST";
    string REAL_CONST = "REAL_CONST";
    string PLUS = "PLUS";
    string MINUS = "MINUS";
    string MUL = "MUL";
    string INTEGER_DIV = "INTEGER_DIV";
    string FLOAT_DIV = "FLOAT_DIV";
    string LPAREN = "LPAREN";
    string RPAREN = "RPAREN";
    string ID = "ID";
    string ASSIGN = "ASSIGN";
    string BEGIN = "BEGIN";
    string END = "END";
    string SEMI = "SEMI";
    string DOT = "DOT";
    string PROGRAM = "PROGRAM";
    string VAR = "VAR";
    string COLON = "COLON";
    string COMMA = "COMMA";
    string PROCEDURE = "PROCEDURE";
    string EOF = "EOF";

    string _type;
    int _value;
    char _val;
    string _valu;


    public Token(string type, int value)
    {
        this._type = type;
        this._value = value;
        this._val = '0';
    }

    public Token(string type, char value)
    {
        this._type = type;
        this._val = value;
    }

    public Token(string type, string value)
    {
        this._type = type;
        this._valu = value;
    }

    public override string ToString()
    {
        if(_val == '0' && _value == 0)
        {
            return string.Format("Token({0}, {1})",
                this._type,
                this._valu
            );
        }
        else if (_val == '0')
        {
            return string.Format("Token({0}, {1})",
                this._type,
                this._value
            );
        }
        else
        {
            return string.Format("Token({0}, {1})",
                this._type,
                this._val
            );
        }

    }
}
