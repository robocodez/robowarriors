﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyUnderSiege : MonoBehaviour
{

    private float count;
    private float currCount;
    // Use this for initialization
    void Start()
    {
        count = 10.0f;
        currCount = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ObjectStorage.getPlayerFlags() != null && currCount >= count && ObjectStorage.getEnemyFlags() != null)
        {
            Instantiate(Resources.Load("EnemyUnderSiege"), transform.position, transform.rotation);
            currCount = 0;
        }
        else
        {
            currCount += 0.2f;
        }
    }
}
