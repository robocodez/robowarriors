﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningConditionShieldBoss : MonoBehaviour {

    private GameObject boss;
	// Use this for initialization
	void Start () {
        boss = GameObject.Find("EnemyShield");
	}
	
	// Update is called once per frame
	void Update () {
		if(boss == null)
        {
            GameManager.resetEnemyList();
        }
	}
}
