﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeletBtnScript : MonoBehaviour {

    private GameObject deleteBtn;
	// Use this for initialization
	void Start () {
        deleteBtn = this.gameObject;
        deleteBtn.GetComponent<Button>().onClick.AddListener(delete);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void delete()
    {
        NewAIConfig.delete();
    }
}
