﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourTutLvl2 : MonoBehaviour {

    private Vector2 velocity;
    private Rigidbody2D rigidbody;
    private float speed = 3.5f;
    private int check;
    // Use this for initialization
    void Start () {
        velocity = Vector2.zero;
        rigidbody = this.GetComponent<Rigidbody2D>();
        check = 0;
        rigidbody.velocity = new Vector2(3.0f, 0f);
    }
	
	// Update is called once per frame
	void Update () {
        //rigidbody.velocity = Vector2.zero;
        if (transform.position.x >= 58 && check % 4 == 0)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(0f, -speed);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;
        }
        else if (transform.position.y <= 27.5 && check % 4 == 1)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(-speed, 0f);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;

        }
        else if (transform.position.x <= 22.8 && check % 4 == 2)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(0f, speed);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;

        }
        else if (transform.position.y >= 44.2 && check % 4 == 3)
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.velocity = new Vector2(speed, 0f);
            this.gameObject.transform.Rotate(0, 0, -90);
            check++;

        }
        else if (GameObject.Find("Player").GetComponent<PlayerTank>().getFlagStatus())
        {
            this.gameObject.GetComponent<EnemyTank>().AddjustCurrentHealth(-100);
            check++;
        }
    }
}
