﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.transform.parent != null && collision.gameObject.transform.parent.gameObject.tag == "Player")
        {
            GameManager.addPlayerTarget(collision.gameObject.transform.parent.gameObject);
            //this.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<EnemyTank>().addPlayerTarget(collision.gameObject.transform.parent.gameObject);
        }
    }
}
