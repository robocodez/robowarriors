﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropdownObject{

    string currCode;
    bool check;

    public DropdownObject(string code)
    {
        currCode = code;
        check = false;
    }

    public string getCurrCode()
    {
        return currCode;
    }

    public void isChecked()
    {
        check = true;
    }

    public bool getChecked()
    {
        return check;
    }
}
