﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavedData : MonoBehaviour {

    private static List<DropdownObject> savedCodes1;
    private static List<DropdownObject> savedCodes2;
    private static List<DropdownObject> savedCodes3;
    private static List<DropdownObject> savedCodes4;
    private static List<DropdownObject> savedCodes5;
    // Use this for initialization
    void Start () {
        savedCodes1 = null;
        savedCodes2 = null;
        savedCodes3 = null;
        savedCodes4 = null;
        savedCodes5 = null;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Still running");
    }

    public static void save(List<DropdownObject> codes1, List<DropdownObject> codes2, List<DropdownObject> codes3, List<DropdownObject> codes4, List<DropdownObject> codes5)
    {
        savedCodes1 = new List<DropdownObject>();
        savedCodes2 = new List<DropdownObject>();
        savedCodes3 = new List<DropdownObject>();
        savedCodes4 = new List<DropdownObject>();
        savedCodes5 = new List<DropdownObject>();
        foreach (DropdownObject code in codes1)
        {
            
            if (code.getChecked())
            {
                code.isChecked();
            }
            savedCodes1.Add(code);
        }
        foreach (DropdownObject code in codes2)
        {
            
            if (code.getChecked())
            {
                code.isChecked();
            }
            savedCodes2.Add(code);
        }
        foreach (DropdownObject code in codes3)
        {
            
            if (code.getChecked())
            {
                code.isChecked();
            }
            savedCodes3.Add(code);
        }
        foreach (DropdownObject code in codes4)
        {
            
            if (code.getChecked())
            {
                code.isChecked();
            }
            savedCodes4.Add(code);
        }
        foreach (DropdownObject code in codes5)
        {
            
            if (code.getChecked())
            {
                code.isChecked();
            }
            savedCodes5.Add(code);
        }
        Debug.Log("Save");
    }

    public static void resetData()
    {
        savedCodes1 = null;
        savedCodes2 = null;
        savedCodes3 = null;
        savedCodes4 = null;
        savedCodes5 = null;
    }

    public static List<DropdownObject> getCodes1()
    {
        return savedCodes1;
    }

    public static List<DropdownObject> getCodes2()
    {
        return savedCodes2;
    }

    public static List<DropdownObject> getCodes3()
    {
        return savedCodes3;
    }

    public static List<DropdownObject> getCodes4()
    {
        return savedCodes4;
    }

    public static List<DropdownObject> getCodes5()
    {
        return savedCodes5;
    }
}
