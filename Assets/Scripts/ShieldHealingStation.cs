﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldHealingStation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("Start");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Heal");
        if(collision.gameObject.tag == "ShieldBit")
        {
            collision.gameObject.GetComponent<ShieldBitScript>().AddjustCurrentHealth(20);
        }
    }
}
