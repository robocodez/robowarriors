﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointSentry : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("collided");
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerTank>().resetHp();
            Debug.Log("Heal");
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            if (collision.gameObject.GetComponent<PlayerTank>().getFlagStatus())
            {
                collision.gameObject.GetComponent<PlayerTank>().updateFlagStatus(false);

            }
        }
    }
}
