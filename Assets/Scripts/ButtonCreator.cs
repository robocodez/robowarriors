﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCreator : MonoBehaviour
{
    public GameObject buttonPrefab;
    public GameObject panelToAttachButtonsTo;
    public GameObject panelToAttachDropdownTo;
    public GameObject goToPostItTo;
    private List<string> list;
    void Start()//Creates a button and sets it up
    {
        //list = new List<string> { "Chase();", "Shoot();", "Goal();", "Move();", "returnSP();", "guardFlags();", "Escape();" };
        GameObject button = (GameObject)Instantiate(buttonPrefab);
        button.transform.SetParent(panelToAttachButtonsTo.transform);//Setting button parent
                                                                     //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                     //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        //button.GetComponent<Dropdown>().AddOptions(list);
        //button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
        //button.transform.GetChild(0).GetComponent<Text>().text = "Set code";//Changing text

    }
    void OnClick()
    {
        Debug.Log("clicked!");
        GameObject button = (GameObject)Instantiate(buttonPrefab);
        button.transform.SetParent(panelToAttachButtonsTo.transform);//Setting button parent
        button.GetComponent<Button>().onClick.AddListener(OnClick);//Setting what button does when clicked
                                                                   //Next line assumes button has child with text as first gameobject like button created from GameObject->UI->Button
        button.transform.GetChild(0).GetComponent<Text>().text = "This is button text";//Changing text
        GameObject choice = (GameObject)Instantiate(panelToAttachDropdownTo);
        choice.transform.SetParent(goToPostItTo.transform);//Setting button parent
    }


}