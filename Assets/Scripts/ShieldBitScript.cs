﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBitScript : MonoBehaviour {

    private int health;
	// Use this for initialization
	void Start () {
        health = 20;
    }
	
	// Update is called once per frame
	void Update () {
        if(health <= 10)
        {
            this.gameObject.transform.parent.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, ((float)153) / 255);
        }
        else
        {
            this.gameObject.transform.parent.gameObject.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, ((float)65) / 255, ((float)153) / 255);
        }
        if(GameObject.Find("EnemyShield") == null)
        {
            AddjustCurrentHealth(-20);
        }
        /*if(GameObject.Find("Enemy Flags") == null)
        {
            AddjustCurrentHealth(-20);
        }*/
        Dead();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            AddjustCurrentHealth(-10);
            
        }
    }

    public void AddjustCurrentHealth(int num)
    {
        health += num;
        if(health > 20)
        {
            health = 20;
            
        }
    }

    void Dead()
    {
        if (health <= 0)
        {
            GameManager.removeTarget(this.gameObject);
            GameObject explode = Instantiate(Resources.Load("Explosion 2"), transform.position, transform.rotation) as GameObject;
            Destroy(explode, 1.0f);
            Destroy(this.gameObject.transform.parent.gameObject);
        }
    }
}
