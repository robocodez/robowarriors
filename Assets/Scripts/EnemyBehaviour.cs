﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    private Vector2 velocity;
    private Rigidbody2D rigidbody;
    private float nf = 0;
    public float missileSpeed;
    private float speed = 3;

    //holds Gameobjects of the missile's target and the closest GameObject to the player
    private static GameObject closestTarget;

    //holds vector values of the distance between distances of targets and the player's current position
    private Vector3 distanceDifference;
    private Vector3 playerPosition;

    //decimal values that hold distances of the player to GameObjects
    private float currentDistance;
    //private float oldDistance;

    //declares the array that holds all the player currently on screen
    private static List<GameObject> playerArray;

    void Awake()
    {
        

        //we set the local variable playerPosition to the player's position by finding the player GameObject by name.
        playerPosition = transform.position;

        //instaniates the hazards' array
        playerArray = new List<GameObject>();


        
    }

    // Use this for initialization
    void Start () {
        velocity = Vector2.zero;
        rigidbody = this.GetComponent<Rigidbody2D>();
        
    }
	
	// Update is called once per frame
	void Update () {
        rigidbody.velocity = Vector2.zero;
        rigidbody.freezeRotation = true;
        playerArray = GameManager.getPlayers();
        //playerArray = this.gameObject.GetComponent<EnemyTank>().getPlayerArray();
        
        /*if(transform.position.x < 25 && transform.position.y > 33)
        {
            velocity = Vector2.zero;
            velocity += new Vector2(0, -5);
        }
        else if(transform.position.x > 50 && transform.position.y < 40)
        {
            velocity = Vector2.zero;
            velocity += new Vector2(0, 5);
        }
        else if (transform.position.y > 40)
        {
            velocity = Vector2.zero;
            velocity += new Vector2(-5, 0);
        }
        else
        {
            velocity = Vector2.zero;
            velocity += new Vector2(5, 0);
        }
        rigidbody.velocity = velocity;
        float angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.Rotate(Vector3.forward * -90);*/
        float Bullet_Forward_Force = 1000;
        GameObject be = (this.gameObject.transform.GetChild(0)).gameObject;
        //player faces target to shoot
        /*float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(new Quaternion(curr.transform.rotation.x, curr.transform.rotation.y, curr.transform.rotation.z, curr.transform.rotation.w), q, Time.deltaTime * speed);*/

        //tells the bullet to be shot forward by a set amount of Bullet_Forward_Force
        /*if (nf >= 10)
        {
            //instantiate and create a bullet object
            GameObject temp_bullet;
            temp_bullet = Instantiate(Resources.Load("Bullet 1 1"), be.transform.position, be.transform.rotation) as GameObject;

            // correct rotation of bullet shot
            // temp_bullet.transform.Rotate(Vector2.left * 90);

            //retrieve the rigidbody component from the instantiated bullet and control it
            Rigidbody2D temp_rigidbody;
            temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
            temp_rigidbody.AddForce(transform.up * Bullet_Forward_Force);
            Debug.Log("Shot");
            nf = 0;
            //ensures the bullet disappears after a set period
            Destroy(temp_bullet, 2.0f);
        }
        else
        {
            nf += 0.5f;
        }*/
        //first we need to check that we have found a Hazard to target at all.  If not, the missile flys straight in the direction its facing
        if (playerArray.Count == 0) {
            //instantiate and create a bullet object
            /*if(nf > 10)
            {
                GameObject temp_bullet;
                temp_bullet = Instantiate(Resources.Load("Bullet 1 1"), be.transform.position, be.transform.rotation) as GameObject;

                //retrieve the rigidbody component from the instantiated bullet and control it
                Rigidbody2D temp_rigidbody;
                temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
                temp_rigidbody.AddForce(transform.right * Bullet_Forward_Force);
                nf = 0;
            }
            else
            {
                nf += 0.5f;
            }*/
            return;
        }

        else
        {
            Vector3 playerPosition = transform.position;
            GameObject closestTarget = null;
            float oldDistance = Mathf.Infinity;
            foreach (GameObject go in playerArray)
            {
                //the first step in the for loop is to find the distance between the player and the current GameObject we are solving for in the Array
                distanceDifference = go.transform.position - playerPosition;

                //we set the float currentDistance to the square magnitude of the distanceDifference.  Since distanceDifference is a Vector 3, sqrMagnitude helps us find the float value
                currentDistance = distanceDifference.sqrMagnitude;

                /*now that we have the currentDistance of the Hazard[i] we need to check three conditions
                1st condition - We need to compare the oldDistance of the last Hazard in the array to the currentDistance of the current Hazard in the array we are solving for
                2nd condition - We need to acces the current Hazard's IsTargetted componet.  It has a bool variable called isTargeted.  If the hazard hasn't been targeted its all good.
                3rd condition - We don't want missiles to launch backwards from the ship.  So we compare the player's position to the hazard's position and make sure the hazards are in about 5 units in front.
                if all 3 of the conditions are true....We have found our new closest Target.*/

                if (currentDistance < oldDistance)
                //go.GetComponent<IsTargeted>().isTargeted == false)
                //&& playerPosition.z < go.transform.position.z - 5)
                {

                    //this sets the closestTarget to the current Hazard we are solving for in our array
                    closestTarget = go;

                    //this sets the oldDistance to the currentDistance for the next cycle through the loop.  If the next Hazard in the array is closer, this will let us know
                    oldDistance = currentDistance;

                    //when we find the closest target we need to change the Hazard's IsTargetted componet to isTargeted true.  This keeps missiles from targetting already targetted Hazards.
                    go.GetComponent<IsTargeted>().isTargeted = true;
                }
            }
            Vector2 vd = closestTarget.transform.position - playerPosition;
            //player faces target to shoot
            
            if (vd.magnitude <= 9)
            {
                float angle = Mathf.Atan2(vd.y, vd.x) * Mathf.Rad2Deg;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w), q, Time.deltaTime * speed);
                if (nf > 10)
                {
                    //instantiate and create a bullet object
                    GameObject temp_bullet;
                    temp_bullet = Instantiate(Resources.Load("Bullet 1 1"), be.transform.position, be.transform.rotation) as GameObject;

                    //retrieve the rigidbody component from the instantiated bullet and control it
                    Rigidbody2D temp_rigidbody;
                    temp_rigidbody = temp_bullet.GetComponent<Rigidbody2D>();
                    temp_rigidbody.AddForce(transform.right * Bullet_Forward_Force);
                    nf = 0;
                    Destroy(temp_bullet, 0.5f);
                }
                else
                {
                    nf += 0.2f;
                }
            }
            
        }
    }

  
}
