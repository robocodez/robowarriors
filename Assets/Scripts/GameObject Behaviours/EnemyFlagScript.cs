﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyFlagScript : MonoBehaviour
{
    private GameObject p1;
    private GameObject p2;
    private GameObject p3;
    private GameObject p4;
    //public Button nextLvlBtn;
    // Use this for initialization
    void Start()
    {
        p1 = GameObject.Find("Player1");
        p2 = GameObject.Find("Player2");
        p3 = GameObject.Find("Player3");
        p4 = GameObject.Find("Player4");
        //nextLvlBtn.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //collision.gameObject.GetComponent<Player1Script>().getRigidBody().velocity = Vector2.zero;
            //collision.gameObject.GetComponent<PlayerTank>().updateFlagStatus(true);
            p1.GetComponent<PlayerTank>().AddjustCurrentHealth(-50);
            p2.GetComponent<PlayerTank>().AddjustCurrentHealth(-50);
            p3.GetComponent<PlayerTank>().AddjustCurrentHealth(-50);
            p4.GetComponent<PlayerTank>().AddjustCurrentHealth(-50);
            Destroy(this.gameObject);

        }
    }
}
