﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTank : MonoBehaviour {

    //initialise variables for health
    public int maxHealth = 100;
    public int curHealth = 100;
    public float healthBarLength;
    public Text text;
    public Boolean hasFlag;
    private Boolean showHp;

    // Use this for initialization
    private static float speed = 3;
    void Start()
    {
        curHealth = maxHealth;
        healthBarLength = Screen.width / 6;
        hasFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        Dead();
    }

    // Enemy tank is destroyed once health is 0 or below
    void Dead()
    {
        if (curHealth <= 0)
        {
            GameManager.removePlayerTarget(this.gameObject);
            text.GetComponent<Text>().text = "Dead";
            GameObject explode = Instantiate(Resources.Load("Explosion"), transform.position, transform.rotation) as GameObject;
            Destroy(explode, 1.0f);
            Destroy(this.gameObject);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            AddjustCurrentHealth(-10);
        }
    }

    public void AddjustCurrentHealth(int adj)
    {
        curHealth += adj;

        if (curHealth < 0)
            curHealth = 0;

        if (curHealth > maxHealth)
            curHealth = maxHealth;

        if (maxHealth < 1)
            maxHealth = 1;

        healthBarLength = (Screen.width / 6) * (curHealth / (float)maxHealth);
    }

    void OnGUI()
    {

        Vector2 targetPos;
        targetPos = Camera.main.WorldToScreenPoint(transform.position);
        //Debug.Log(ObjectStorage.getTrue());
        if(ObjectStorage.getTrue())
        {
            GUI.Box(new Rect(targetPos.x, Screen.height - targetPos.y, 60, 20), curHealth + "/" + maxHealth);
            GUI.depth = 0;
        }
        

    }

    public int getHp()
    {
        return curHealth;
    }

    public void resetHp()
    {
        curHealth = maxHealth;
    }

    public float getSpeed()
    {
        return speed;
    }

    public int getMaxHp()
    {
        return maxHealth;
    }


    public Boolean getFlagStatus()
    {
        return hasFlag;
    }

    public void updateFlagStatus(Boolean status)
    {
        hasFlag = status;
    }

}
