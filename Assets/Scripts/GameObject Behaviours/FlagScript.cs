﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlagScript : MonoBehaviour {

    public Button nextLvlBtn;
    public GameObject victory;
    public GameObject effect1;
    public GameObject effect2;
    public GameObject effect3;
    // Use this for initialization
    void Start () {
        nextLvlBtn.gameObject.SetActive(false);
        victory.SetActive(false);
        effect1.SetActive(false);
        effect2.SetActive(false);
        effect3.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player1Script>().getRigidBody().velocity = Vector2.zero;
            nextLvlBtn.gameObject.SetActive(true);
            victory.SetActive(true);
            effect1.SetActive(true);
            effect2.SetActive(true);
            effect3.SetActive(true);
            collision.gameObject.GetComponent<PlayerTank>().updateFlagStatus(true);
            Destroy(this.gameObject);
            
        }
    }
}
