﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : MonoBehaviour {

    //initialise variables for health
    public int maxHealth = 100;
    public int curHealth = 100;
    public float healthBarLength;
    // Use this for initialization
    private List<GameObject> playerArray;
    void Start() {
        curHealth = maxHealth;
        healthBarLength = Screen.width / 6;
        playerArray = new List<GameObject>();
    }

    // Update is called once per frame
    void Update() {
        Dead();
    }

    // Enemy tank is destroyed once health is 0 or below
    void Dead()
    {
        if (curHealth <= 0)
        {
            GameManager.removeTarget(this.gameObject);
            GameObject explode = Instantiate(Resources.Load("Explosion"), transform.position, transform.rotation) as GameObject;
            Destroy(explode, 1.0f);
            Destroy(this.gameObject);
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            AddjustCurrentHealth(-10);
            Debug.Log("Hit");
        }
    }

    public void AddjustCurrentHealth(int adj)
    {
        curHealth += adj;

        if (curHealth < 0)
            curHealth = 0;

        if (curHealth > maxHealth)
            curHealth = maxHealth;

        if (maxHealth < 1)
            maxHealth = 1;

        healthBarLength = (Screen.width / 6) * (curHealth / (float)maxHealth);
    }

    void OnGUI()
    {

        Vector2 targetPos;
        targetPos = Camera.main.WorldToScreenPoint(transform.position);
        if (ObjectStorage.getTrue())
        {
            GUI.Box(new Rect(targetPos.x, Screen.height - targetPos.y, 60, 20), curHealth + "/" + maxHealth);
        }
        

    }

    public void addPlayerTarget(GameObject go)
    {
        playerArray.Add(go);
    }

    public List<GameObject> getPlayerArray()
    {
        return playerArray;
    }
}
