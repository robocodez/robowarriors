﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;

public class ButtonManager : MonoBehaviour {

	public void newTutorial()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialMenu3D");
    }

    public void newCampaign()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("CampaignMenu3D");
    }

    public void newUnderSiege()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("UnderSiege");
    }

    public void newTutLvl1()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl1");
    }

    public void newTutLvl2()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl2");
    }

    public void newTutLvl3()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl3");
    }

    public void newTutLvl4()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl4");
    }

    public void newTutLvl5()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl5");
    }

    public void newTutLvl6()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TutorialLvl6");
    }

    public void newCampaignLvl2()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("UnderSiege");
    }

    public void newCampaignLvl3()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Surrounded");
    }

    public void newCampaignLvl4()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Sentry");
    }

    public void newCredits()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Credits3D");
    }

    public void backToMain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu3D");
    }


    void Start()
    {
        MusicScript.changeMusic(1);
    }

    void Update()
    {

    }
}
